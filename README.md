# Millions Saved

The Millions Saved app is a universal javascript app, using the following technologies:

- Keystone
	- Express
	- Mongo db (Mongoose)
- React
- React-router
- Redux
- Fetchr

The app connects to a mongo db database and the build is handled with gulp and webpack, also using babel for es2015 feature compatibility.

## Getting Started (local)

To get started clone the repo and run `npm install`. Follow the steps below to connect the database and get the app running in dev mode.

1. Go to `./updates` and create the first update. See the update user admin section below. If there is no updates directory, you can create one at `./updates`.
2. Run `gulp`. This will build the js source for the client-side code.
3. Make a .env file with the following variables: `MONGO_URI` (mongolab uri), `S3_BUCKET`, `S3_KEY`, `S3_SECRET`, `S3_REGION`, `CLOUDINARY_CLOUD_NAME`, `CLOUDINARY_API_KEY`, `CLOUDINARY_API_SECRET`
4. Open another terminal window and run `gulp start`. This will run keystone using nodemon. You will not have to restart keystone when changing client-side code. Since this is a universal js app, a lot of the code is shared. If the code is not in-sync react will issue a warning in the browser console.
5. Keystone will start up and create the database, along with your admin user
6. It would be good to add the `0.0.1-admin` file to `.gitignore`.
7. Go to `localhost:3000` and you should see a very bare page with no styling and a login link. Once you login you will see the keystone interface.

Note that the .env file should be ignored when pushing to github. For deployment on heroku you will have to set up these variables on the server. You can do this either via terminal or via the heroku GUI. Anything from the .env file should be available as environment variables on the heroku server.

Also note that in order to prepare the app for production, you only need to run gulp prepare. This will minify the client-side code. Please be patient, as the preparation process can take up to a minute.

## Update User Admin

To make keystone accessible via the admin interface, you have to setup an initial user. To do that go to `./update`, and create a new file called `0.0.1-admins.js`. The following needs to go into that file:

  exports.create = {
    User: [
      { 'name.first': 'Admin', 'name.last': 'User', email: 'YOUR EMAIL', password: 'admin', isAdmin: true }
    ]
  };

## app structure

The app shares most code between the client and the server. This makes some processes tricky (e.g. data fetching). To solve this we use fetchr, which determines whether we are fetching data from the server directly or from the client via xhr and an api endpoint.

For routing we use react-router both on the server and on the client side. Redux handles all of our action dispatching and provides a flux architecture without much overhead.

## Build

The app build is handled by gulp and webpack. Note that when working with gulp in development, make sure that you check that the `devEnv` variable in the gulpfile is set to true. The gulpfile is only responsible for compiling scss to css. All javascript builds are handled by webpack. Use the following commands:

###`$ gulp`

Compiles all styles and watches for changes to the scss files.

###`$ webpack`

Builds all client-side js once.

###`$ webpack --watch`

Builds all client side js and watches for changes to js files. The first build is slow, but all following builds are very fast, due to incremental builds.

###`$ webpack -p`

Use this command to build the client-side js for production. **Make sure to run this before you deploy!**

###`$ webpack -d`

Use this command to compile js with sourcemaps.
