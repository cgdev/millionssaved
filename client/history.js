
import createBrowserHistory from 'history/lib/createBrowserHistory';

var history = createBrowserHistory();

export default history;
