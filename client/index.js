
import ga from 'ga-react-router';

import React from 'react';
import ReactDOM from 'react-dom';
import Fetcher from 'fetchr';

import { Router, match } from 'react-router';
import routes from '../shared/routes';

import { createStore, combineReducers, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';

import caseStudiesReducer from '../shared/reducers/caseStudiesReducer';
import kenyaVisualizationReducer from '../shared/reducers/kenyaVisualizationReducer';
import menAfriVacVisualizationReducer from '../shared/reducers/menAfriVacVisualizationReducer';
import mapReducer from '../shared/reducers/mapReducer';
import mediaItemsReducer from '../shared/reducers/mediaItemsReducer';

import fetchNeededData from '../shared/lib/needsFetcher';

import Minigrid from 'minigrid';
window.Minigrid = Minigrid;

import history from './history';
window.historyHack = history;

let initialState = window.__INITIAL_STATE__;

var transitionComplete = false, transitionOverlay, viewWrapper, loader;

var fetcher = new Fetcher({
  xhrPath: '/api',
  xhrTimeout: 10000
});

window.fetcher = fetcher;

// var firstRender = true;
var subviewTransition = false;

const store = applyMiddleware(thunk)(createStore)(combineReducers({caseStudiesReducer, kenyaVisualizationReducer, menAfriVacVisualizationReducer, mapReducer, mediaItemsReducer}), initialState);

window.scrollMagicMasterController = new ScrollMagic.Controller();

ReactDOM.render(
	<Provider store={store}>
		{() => <Router children={routes} history={history} />}
	</Provider>,
	document.getElementById('react-view'),
	() => {
		transitionOverlay = document.getElementById('transition-overlay');
		viewWrapper = document.getElementById('view-wrapper');
		loader = document.getElementById('loader');
	}
);

// if (window) {
// 	window.render = (depth, breadth, hash) => {
// 		ReactDOMStream.render(
// 			<Provider store={store}>
// 				{() => <Router children={routes} history={history} />}
// 			</Provider>,
// 			document.getElementById("renderNode"),
// 			hash
// 		);
// 	}
// }

// var dataFetchingPromise;

history.listen((location) => {

	window.ga('send', 'pageview', location.pathname);
    
	if(location.action == 'PUSH') {

		if(subviewTransition) {
			var subview = document.getElementById('subview');

			window.requestAnimationFrame(() => {
				Velocity(subview, {
					translateZ: 0,
					opacity: [1, 0]
				}, {
					duration: 400,
					mobileHA: true,
					easing: [ 0.77, 0, 0.175, 1 ]
				});
			});

			subviewTransition = false;

			return;
		}

		window.scrollTo(0, 0);

		// setTimeout(() => {
			window.requestAnimationFrame(() => {
				Velocity(viewWrapper, {
					translateZ: 0,
					opacity: [ 1, 0 ],
					translateY: [ 0, -60 ]
				}, {
					duration: 1000,
					mobileHA: true,
					easing: [ 0.77, 0, 0.175, 1 ],
					complete: () => {
						// Fix css bug where transform property on view wrapper
						// causes case study sticky navigation to behave like
						// absolute position instead of fixed...
						// Check for cross-browser compatibility...
						viewWrapper.style.removeProperty('-webkit-transform');
						viewWrapper.style.removeProperty('-moz-transform');
						viewWrapper.style.removeProperty('-ms-transform');
						viewWrapper.style.removeProperty('-o-transform');
						viewWrapper.style.removeProperty('transform');
					}
				});
				Velocity(transitionOverlay, {
			  	translateZ: 0,
			  	translateY: [ '100%', 0 ]
			  }, {
			  	easing: [ 0.77, 0, 0.175, 1 ],
			  	duration: 1000,
			  	mobileHA: true
			  });
			});
		// }, 100);
  }
});

history.listenBefore((location, callback) => {
  if(location.action == 'POP') {
  	callback();
  	return;
  }

  match({ routes, location }, (error, redirectLocation, renderProps) => {
		var dataFetchingPromise = fetchNeededData(store.dispatch, renderProps.components, renderProps.params, fetcher);

		if(window.location.pathname.split('/')[1] == 'about' && window.location.pathname.split('/')[1] == renderProps.location.pathname.split('/')[1]) {
			var subview = document.getElementById('subview');
			subviewTransition = true;

			window.requestAnimationFrame(() => {
				Velocity(subview, {
					translateZ: 0,
					opacity: [0, 1]
				}, {
					duration: 400,
					mobileHA: true,
					easing: [ 0.77, 0, 0.175, 1 ],
					complete: () => {
						callback();
					}
				});
			});

			return;
		}

  	// 	dataFetchingPromise.then(() => {
  	// 		viewWrapper.style.opacity = 0;
		// 	callback();
		// });

		window.requestAnimationFrame(() => {
			Velocity(viewWrapper, {
				translateZ: 0,
				opacity: [ 0, 1 ],
				translateY: [ 60, 0 ]
			}, {
				duration: 600,
				mobileHA: true,
				easing: [ 0.77, 0, 0.175, 1 ]
			});
			Velocity(transitionOverlay, {
				translateZ: 0,
		  	translateY: [ 0, '-100%' ]
		  }, {
		  	duration: 600,
		  	mobileHA: true,
		  	easing: [ 0.77, 0, 0.175, 1 ],
		  	complete: () => {
		  		loader.className = 'loader loader--active';
					dataFetchingPromise.then(() => {
						loader.className = 'loader';
						callback();
					})
					.catch((err) => {
						console.log('Error found: ', err);
					});
		  	}
		  });
		});
	});

});
