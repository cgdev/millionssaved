
import {invalidateTemplates} from '../routes/templateCache';

import keystone from 'keystone';

var Types = keystone.Field.Types;

import {uploadImage, deleteImage} from '../utils/uploadUtils';

var CaseStudy = new keystone.List('CaseStudy', {
	autokey: { path: 'slug', from: 'title', unique: true },
	map: { name: 'title' },
	defaultSort: '-createdAt'
});

CaseStudy.add({
	preTitle: { type: String, initial: true },
	title: { type: String, required: true },
	state: { type: Types.Select, options: 'draft, published, archived', default: 'draft' },
	caseType: {
		type: Types.Select,
		numeric: true,
		options: [
			{ value: 0, label: 'Basic Case Study'    },
			{ value: 1, label: 'Extended Case Study' },
		],
		initial: true,
		required: true,
		emptyOption: false
	},
	image: {
    type: Types.S3File,
    headers: {
      'x-amz-meta-Cache-Control' : 'max-age=' + (60 * 30)
    },
    dependsOn: { caseType: 0 }
  },
  imageCredit: {
  	name: { type: String    },
  	link: { type: Types.Url }
  },
	template: {
		type: Types.Select,
		numeric: true,
		options: [
			{ value: 0, label: 'MenAfriVac' },
			{ value: 1, label: 'Botswana ARV' },
			{ value: 2, label: 'Argentina Plan Nacer' },
			{ value: 3, label: 'Thailand UCS' },
			{ value: 4, label: 'Kenya OVC' },
			{ value: 5, label: 'South Africa CSG' },
			{ value: 6, label: 'Vietnam Helmets' },
			{ value: 7, label: 'Indonesia Sanitation' },
			{ value: 8, label: 'Thailand Tobacco' },
			{ value: 9, label: 'Haiti Polio' }
		],
		dependsOn: { caseType: 1 }
	},

	author: { type: String },

	createdAt: { type: Date, default: Date.now, hidden: true },
	publishedAt: Date,

	location: {
		lat: { type: String },
		lon: { type: String, note: 'Coordinates of countries can be easily obtained via google. Use the North/South value for lat, and East/West value for lon. Positive numbers indicate North and East, and negative numbers indicate South and West.' }
	},

	highlightCountries: {
		type: Types.TextArray
	},
	
	intro: { type: Types.Textarea, initial: true },
	visualizationsLoaded: { type: Types.Boolean, hidden: true, default: false }
}, {
	heading: 'Basic Case Study',
	dependsOn: { caseType: 0 }
}, {
	basic: {
		lead: {
			type: Types.Html, wysiwyg: true, dependsOn: { caseType: 0 },
			height: 300
		},
		textOne: {
			type: Types.Html, wysiwyg: true, dependsOn: { caseType: 0 },
			height: 300
		},
		quote: {
			type: Types.Textarea, dependsOn: { caseType: 0 }
		},
		textTwo: {
			type: Types.Html, wysiwyg: true, dependsOn: { caseType: 0 },
			height: 300
		}
	}
}, {
	heading: 'Summary',
	dependsOn: { caseType: 1 }
}, {
	summary: {
		approach:     { type: Types.Markdown, dependsOn: { caseType: 1 } },
		impact:       { type: Types.Markdown, dependsOn: { caseType: 1 } },
		cost:         { type: Types.Markdown, dependsOn: { caseType: 1 } },
		costPopup:    { type: Types.Textarea, dependsOn: { caseType: 1 } },
		text:         { type: Types.Textarea, dependsOn: { caseType: 1 } },
		approachText: { type: Types.Textarea, dependsOn: { caseType: 1 }, label: 'Approach text' },
		impactText:   { type: Types.Textarea, dependsOn: { caseType: 1 }, label: 'Impact text'   }
	}
}, {
	heading: 'Background',
	dependsOn: { caseType: 1 }
}, {
	lead: { type: Types.Textarea, dependsOn: { caseType: 1 } },
	background: {
		paragraphOne:   { type: Types.Html, wysiwyg: true, height: 200, dependsOn: { caseType: 1 } },
		paragraphTwo:   { type: Types.Html, wysiwyg: true, height: 200, collapse: true, dependsOn: { 'background.paragraphOne': true, caseType: 1 } },
		paragraphThree: { type: Types.Html, wysiwyg: true, height: 200, collapse: true, dependsOn: { 'background.paragraphTwo': true, caseType: 1 } },
		paragraphFour:  { type: Types.Html, wysiwyg: true, height: 200, collapse: true, dependsOn: { 'background.paragraphThree': true, caseType: 1 } },
		paragraphFive:  { type: Types.Html, wysiwyg: true, height: 200, collapse: true, dependsOn: { 'background.paragraphFour': true, caseType: 1 } },

		visualizationOne:   { type: Types.Markdown, collapse: true, dependsOn: { caseType: 1 } },
		visualizationTwo:   { type: Types.Markdown, collapse: true, dependsOn: { 'background.visualizationOne': true, caseType: 1 } },
		visualizationThree: { type: Types.Markdown, collapse: true, dependsOn: { 'background.visualizationTwo': true, caseType: 1 } },
		visualizationFour:  { type: Types.Markdown, collapse: true, dependsOn: { 'background.visualizationThree': true, caseType: 1 } },
		visualizationFive:  { type: Types.Markdown, collapse: true, dependsOn: { 'background.visualizationFour': true, caseType: 1 } },
		visualizationSix:   { type: Types.Markdown, collapse: true, dependsOn: { 'background.visualizationFive': true, caseType: 1 } }
	}
}, {
	heading: 'Break',
	dependsOn: { caseType: 1 }
}, {
	breakOne: {
		isQuote: { type: Types.Boolean, dependsOn: { caseType: 1 } },
		tweetable: { type: Types.Boolean, dependsOn: { caseType: 1 } },
		lg: { type: Types.Html, wysiwyg: true, collapse: true, dependsOn: { caseType: 1 } },
		md: { type: Types.Html, wysiwyg: true, collapse: true, dependsOn: { caseType: 1 } },
		sm: { type: Types.Html, wysiwyg: true, collapse: true, dependsOn: { caseType: 1 } }
	}
}, {
	heading: 'Program Rollout',
	dependsOn: { caseType: 1 }
}, {
	programRollout: {
		paragraphOne:      { type: Types.Html, wysiwyg: true, height: 200, dependsOn: { caseType: 1 } },
		paragraphTwo:      { type: Types.Html, wysiwyg: true, height: 200, collapse: true, dependsOn: { 'programRollout.paragraphOne': true, caseType: 1 } },
		paragraphThree:    { type: Types.Html, wysiwyg: true, height: 200, collapse: true, dependsOn: { 'programRollout.paragraphTwo': true, caseType: 1 } },
		paragraphFour:     { type: Types.Html, wysiwyg: true, height: 200, collapse: true, dependsOn: { 'programRollout.paragraphThree': true, caseType: 1 } },
		paragraphFive:     { type: Types.Html, wysiwyg: true, height: 200, collapse: true, dependsOn: { 'programRollout.paragraphFour': true, caseType: 1 } },
		paragraphSix:      { type: Types.Html, wysiwyg: true, height: 200, collapse: true, dependsOn: { 'programRollout.paragraphFive': true, caseType: 1 } },
		paragraphSeven:    { type: Types.Html, wysiwyg: true, height: 200, collapse: true, dependsOn: { 'programRollout.paragraphSix': true, caseType: 1 } },
		paragraphEight:    { type: Types.Html, wysiwyg: true, height: 200, collapse: true, dependsOn: { 'programRollout.paragraphSeven': true, caseType: 1 } },
		paragraphNine:     { type: Types.Html, wysiwyg: true, height: 200, collapse: true, dependsOn: { 'programRollout.paragraphEight': true, caseType: 1 } },
		paragraphTen:      { type: Types.Html, wysiwyg: true, height: 200, collapse: true, dependsOn: { 'programRollout.paragraphNine': true, caseType: 1 } },
		paragraphEleven:   { type: Types.Html, wysiwyg: true, height: 200, collapse: true, dependsOn: { 'programRollout.paragraphTen': true, caseType: 1 } },
		paragraphTwelve:   { type: Types.Html, wysiwyg: true, height: 200, collapse: true, dependsOn: { 'programRollout.paragraphEleven': true, caseType: 1 } },
		paragraphThirteen: { type: Types.Html, wysiwyg: true, height: 200, collapse: true, dependsOn: { 'programRollout.paragraphTwelve': true, caseType: 1 } },

		visualizationOne:   { type: Types.Markdown, collapse: true, dependsOn: { caseType: 1 } },
		visualizationTwo:   { type: Types.Markdown, collapse: true, dependsOn: { 'programRollout.visualizationOne': true, caseType: 1   } },
		visualizationThree: { type: Types.Markdown, collapse: true, dependsOn: { 'programRollout.visualizationTwo': true, caseType: 1   } },
		visualizationFour:  { type: Types.Markdown, collapse: true, dependsOn: { 'programRollout.visualizationThree': true, caseType: 1 } },
		visualizationFive:  { type: Types.Markdown, collapse: true, dependsOn: { 'programRollout.visualizationFour': true, caseType: 1  } },
		visualizationSix:   { type: Types.Markdown, collapse: true, dependsOn: { 'programRollout.visualizationFive': true, caseType: 1  } },
		visualizationSeven: { type: Types.Markdown, collapse: true, dependsOn: { 'programRollout.visualizationSix': true, caseType: 1   } },
		visualizationEight: { type: Types.Markdown, collapse: true, dependsOn: { 'programRollout.visualizationSeven': true, caseType: 1 } },
		visualizationNine:  { type: Types.Markdown, collapse: true, dependsOn: { 'programRollout.visualizationEight': true, caseType: 1 } }
	}
}, {
	heading: 'Break',
	dependsOn: { caseType: 1 }
}, {
	breakTwo: {
		isQuote: { type: Types.Boolean, dependsOn: { caseType: 1 } },
		tweetable: { type: Types.Boolean, dependsOn: { caseType: 1 } },
		lg: { type: Types.Html, wysiwyg: true, collapse: true, dependsOn: { caseType: 1 } },
		md: { type: Types.Html, wysiwyg: true, collapse: true, dependsOn: { caseType: 1 } },
		sm: { type: Types.Html, wysiwyg: true, collapse: true, dependsOn: { caseType: 1 } }
	}
}, {
	heading: 'Impact',
	dependsOn: { caseType: 1 }
}, {
	impact: {
		paragraphOne:   { type: Types.Html, wysiwyg: true, height: 200, dependsOn: { caseType: 1 } },
		paragraphTwo:   { type: Types.Html, wysiwyg: true, height: 200, collapse: true, dependsOn: { 'impact.paragraphOne': true, caseType: 1 } },
		paragraphThree: { type: Types.Html, wysiwyg: true, height: 200, collapse: true, dependsOn: { 'impact.paragraphTwo': true, caseType: 1 } },
		paragraphFour:  { type: Types.Html, wysiwyg: true, height: 200, collapse: true, dependsOn: { 'impact.paragraphThree': true, caseType: 1 } },
		paragraphFive:  { type: Types.Html, wysiwyg: true, height: 200, collapse: true, dependsOn: { 'impact.paragraphFour': true, caseType: 1 } },

		visualizationOne:   { type: Types.Markdown, collapse: true, dependsOn: { caseType: 1 } },
		visualizationTwo:   { type: Types.Markdown, collapse: true, dependsOn: { 'impact.visualizationOne': true, caseType: 1 } },
		visualizationThree: { type: Types.Markdown, collapse: true, dependsOn: { 'impact.visualizationTwo': true, caseType: 1 } },
		visualizationFour:  { type: Types.Markdown, collapse: true, dependsOn: { 'impact.visualizationThree': true, caseType: 1 } },
		visualizationFive:  { type: Types.Markdown, collapse: true, dependsOn: { 'impact.visualizationFour': true, caseType: 1 } },
		visualizationSix:   { type: Types.Markdown, collapse: true, dependsOn: { 'impact.visualizationFive': true, caseType: 1 } }
	}
}, {
	heading: 'Main Visualization',
	dependsOn: { caseType: 1 }
}, {
	mainVisualizationSource: {
		type: Types.Html, wysiwyg: true, dependsOn: { caseType: 1 }
	}
}, {
	heading: 'Break',
	dependsOn: { caseType: 1 }
}, {
	breakThree: {
		isQuote: { type: Types.Boolean, dependsOn: { caseType: 1 } },
		tweetable: { type: Types.Boolean, dependsOn: { caseType: 1 } },
		lg: { type: Types.Html, wysiwyg: true, collapse: true, dependsOn: { caseType: 1 } },
		md: { type: Types.Html, wysiwyg: true, collapse: true, dependsOn: { caseType: 1 } },
		sm: { type: Types.Html, wysiwyg: true, collapse: true, dependsOn: { caseType: 1 } }
	}
}, {
	heading: 'Cost',
	dependsOn: { caseType: 1 }
}, {
	cost: {
		paragraphOne:   { type: Types.Html, wysiwyg: true, height: 200, dependsOn: { caseType: 1 } },
		paragraphTwo:   { type: Types.Html, wysiwyg: true, height: 200, collapse: true, dependsOn: { 'cost.paragraphOne': true, caseType: 1 } },
		paragraphThree: { type: Types.Html, wysiwyg: true, height: 200, collapse: true, dependsOn: { 'cost.paragraphTwo': true, caseType: 1 } },
		paragraphFour:  { type: Types.Html, wysiwyg: true, height: 200, collapse: true, dependsOn: { 'cost.paragraphThree': true, caseType: 1 } },
		paragraphFive:  { type: Types.Html, wysiwyg: true, height: 200, collapse: true, dependsOn: { 'cost.paragraphFour': true, caseType: 1 } },

		visualizationOne:   { type: Types.Markdown, collapse: true, dependsOn: { caseType: 1 } },
		visualizationTwo:   { type: Types.Markdown, collapse: true, dependsOn: { 'cost.visualizationOne': true, caseType: 1 } },
		visualizationThree: { type: Types.Markdown, collapse: true, dependsOn: { 'cost.visualizationTwo': true, caseType: 1 } },
		visualizationFour:  { type: Types.Markdown, collapse: true, dependsOn: { 'cost.visualizationThree': true, caseType: 1 } },
		visualizationFive:  { type: Types.Markdown, collapse: true, dependsOn: { 'cost.visualizationFour': true, caseType: 1 } },
		visualizationSix:   { type: Types.Markdown, collapse: true, dependsOn: { 'cost.visualizationFive': true, caseType: 1 } }
	}
}, {
	heading: 'Break',
	dependsOn: { caseType: 1 }
}, {
	breakFour: {
		isQuote: { type: Types.Boolean, dependsOn: { caseType: 1 } },
		tweetable: { type: Types.Boolean, dependsOn: { caseType: 1 } },
		lg: { type: Types.Html, wysiwyg: true, collapse: true, dependsOn: { caseType: 1 } },
		md: { type: Types.Html, wysiwyg: true, collapse: true, dependsOn: { caseType: 1 } },
		sm: { type: Types.Html, wysiwyg: true, collapse: true, dependsOn: { caseType: 1 } }
	}
}, {
	heading: 'Reasons For Success',
	dependsOn: { caseType: 1 }
}, {
	reasonsForSuccess: {
		paragraphOne:    { type: Types.Html, wysiwyg: true, height: 200, dependsOn: { caseType: 1 } },
		paragraphTwo:    { type: Types.Html, wysiwyg: true, height: 200, collapse: true, dependsOn: { 'reasonsForSuccess.paragraphOne': true, caseType: 1 } },
		paragraphThree:  { type: Types.Html, wysiwyg: true, height: 200, collapse: true, dependsOn: { 'reasonsForSuccess.paragraphTwo': true, caseType: 1 } },
		paragraphFour:   { type: Types.Html, wysiwyg: true, height: 200, collapse: true, dependsOn: { 'reasonsForSuccess.paragraphThree': true, caseType: 1 } },
		paragraphFive:   { type: Types.Html, wysiwyg: true, height: 200, collapse: true, dependsOn: { 'reasonsForSuccess.paragraphFour': true, caseType: 1 } },
		paragraphSix:    { type: Types.Html, wysiwyg: true, height: 200, collapse: true, dependsOn: { 'reasonsForSuccess.paragraphFive': true, caseType: 1 } },
		paragraphSeven:  { type: Types.Html, wysiwyg: true, height: 200, collapse: true, dependsOn: { 'reasonsForSuccess.paragraphSix': true, caseType: 1 } },
		paragraphEight:  { type: Types.Html, wysiwyg: true, height: 200, collapse: true, dependsOn: { 'reasonsForSuccess.paragraphSeven': true, caseType: 1 } },

		visualizationOne:   { type: Types.Markdown, collapse: true, dependsOn: { caseType: 1 } },
		visualizationTwo:   { type: Types.Markdown, collapse: true, dependsOn: { 'reasonsForSuccess.visualizationOne': true, caseType: 1 } },
		visualizationThree: { type: Types.Markdown, collapse: true, dependsOn: { 'reasonsForSuccess.visualizationTwo': true, caseType: 1 } },
		visualizationFour:  { type: Types.Markdown, collapse: true, dependsOn: { 'reasonsForSuccess.visualizationThree': true, caseType: 1 } },
		visualizationFive:  { type: Types.Markdown, collapse: true, dependsOn: { 'reasonsForSuccess.visualizationFour': true, caseType: 1 } },
		visualizationSix:   { type: Types.Markdown, collapse: true, dependsOn: { 'reasonsForSuccess.visualizationFive': true, caseType: 1 } }
	}
}, {
	heading: 'Break',
	dependsOn: { caseType: 1 }
}, {
	breakFive: {
		isQuote: { type: Types.Boolean, dependsOn: { caseType: 1 } },
		tweetable: { type: Types.Boolean, dependsOn: { caseType: 1 } },
		lg: { type: Types.Html, wysiwyg: true, collapse: true, dependsOn: { caseType: 1 } },
		md: { type: Types.Html, wysiwyg: true, collapse: true, dependsOn: { caseType: 1 } },
		sm: { type: Types.Html, wysiwyg: true, collapse: true, dependsOn: { caseType: 1 } }
	}
}, {
	heading: 'Implications',
	dependsOn: { caseType: 1 }
}, {
	implications: {
		paragraphOne:   { type: Types.Html, wysiwyg: true, height: 200, dependsOn: { caseType: 1 } },
		paragraphTwo:   { type: Types.Html, wysiwyg: true, height: 200, collapse: true, dependsOn: { 'implications.paragraphOne': true, caseType: 1 } },
		paragraphThree: { type: Types.Html, wysiwyg: true, height: 200, collapse: true, dependsOn: { 'implications.paragraphTwo': true, caseType: 1 } },
		paragraphFour:  { type: Types.Html, wysiwyg: true, height: 200, collapse: true, dependsOn: { 'implications.paragraphThree': true, caseType: 1 } },
		paragraphFive:  { type: Types.Html, wysiwyg: true, height: 200, collapse: true, dependsOn: { 'implications.paragraphFour': true, caseType: 1 } },
		paragraphSix:   { type: Types.Html, wysiwyg: true, height: 200, collapse: true, dependsOn: { 'implications.paragraphFive': true, caseType: 1 } },
		paragraphSeven: { type: Types.Html, wysiwyg: true, height: 200, collapse: true, dependsOn: { 'implications.paragraphSix': true, caseType: 1 } },

		visualizationOne:   { type: Types.Markdown, collapse: true, dependsOn: { caseType: 1 } },
		visualizationTwo:   { type: Types.Markdown, collapse: true, dependsOn: { 'implications.visualizationOne': true, caseType: 1 } },
		visualizationThree: { type: Types.Markdown, collapse: true, dependsOn: { 'implications.visualizationTwo': true, caseType: 1 } },
		visualizationFour:  { type: Types.Markdown, collapse: true, dependsOn: { 'implications.visualizationThree': true, caseType: 1 } },
		visualizationFive:  { type: Types.Markdown, collapse: true, dependsOn: { 'implications.visualizationFour': true, caseType: 1 } },
		visualizationSix:   { type: Types.Markdown, collapse: true, dependsOn: { 'implications.visualizationFive': true, caseType: 1 } }
	}
}, {
	heading: 'Conclusion',
	dependsOn: { caseType: 1 }
}, {
	conclusion: {
		lg: { type: Types.Html, wysiwyg: true, dependsOn: { caseType: 1 } },
		md: { type: Types.Html, wysiwyg: true, collapse: true, dependsOn: { caseType: 1 } },
		sm: { type: Types.Html, wysiwyg: true, collapse: true, dependsOn: { caseType: 1 } }
	}
}, {
	heading: 'Taxonomy'
}, {
	region:   { type: Types.Select, options: 'Africa, South-East Asia, Europe, Oceania, Latin America', default: 'Africa', initial: true },
	approach: { type: Types.Select, options: 'Medical Rollout and Technologies, Expanding Access to Health Services, Cash Transfers to Improve Health, Behavior Change to Decrease Risk', default: 'Medical Rollout and Technologies', initial: true }
}
);

CaseStudy.schema.post('init', function() {
	this._image_prev = this.image.toObject();
});

CaseStudy.fields.image.pre('upload', function(item, file, next) {
  uploadImage({
    item: item,
    file: file,
    next: next
  });
});

CaseStudy.schema.pre('save', function(next) {
	if(this.isModified('image') && this._image_prev) {
		console.log(this._image_prev);
		deleteImage({
		  file: this._image_prev,
		  next: next
		});
	}
	else {
		next();
	}
});

CaseStudy.schema.post('save', function() {
  invalidateTemplates();
});

CaseStudy.defaultColumns = 'title|30%, state|15%, intro|40%, publishedAt|15%';

CaseStudy.register();
