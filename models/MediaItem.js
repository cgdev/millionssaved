
import {invalidateTemplates} from '../routes/templateCache';

import keystone from 'keystone';

var Types = keystone.Field.Types;

import {uploadImage, deleteImage} from '../utils/uploadUtils';
 
var MediaItem = new keystone.List('MediaItem', {
  autokey: { path: 'slug', from: 'title', unique: true },
  map: { name: 'title' },
  defaultSort: '-createdAt'
});
 
MediaItem.add({
  title: { type: String, required: true },
  state: { type: Types.Select, options: 'draft, published, archived', default: 'draft' },
  createdAt: { type: Date, default: Date.now, hidden: true },
  publishedAt: Date,
  intro: { type: Types.Textarea },
  // image: {
  //   type: Types.S3File,
  //   headers: {
  //     'x-amz-meta-Cache-Control' : 'max-age=' + (60 * 30)
  //   },
  //   note: 'Please do not upload images yet.'
  // },
  source: {
    name: { type: String },
    url: { type: Types.Url },
    publishedAt: Date
  }
});

// MediaItem.schema.post('init', function() {
//  this._image_prev = this.image.toObject();
// });

// MediaItem.fields.image.pre('upload', function(item, file, next) {
//   uploadImage({
//     item: item,
//     file: file,
//     next: next
//   });
// });

// MediaItem.schema.pre('save', function(next) {
//  if(this.isModified('image')) {
//   deleteImage({
//     file: this._image_prev,
//     next: next
//   });
//  }
//  else {
//   next();
//  }
// });

MediaItem.schema.post('save', function() {
  invalidateTemplates();
});
 
MediaItem.defaultColumns = 'title, state|20%, publishedAt|15%, source.name';

MediaItem.register();
