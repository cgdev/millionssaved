
import Fetcher  from 'fetchr';
import React    from 'react';

import createLocation from 'history/lib/createLocation';
import { match } from 'react-router';
import routes   from '../shared/routes';

import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunk           from 'redux-thunk';

import caseStudiesReducer     from '../shared/reducers/caseStudiesReducer';
import kenyaVisualizationReducer from '../shared/reducers/kenyaVisualizationReducer';
import menAfriVacVisualizationReducer from '../shared/reducers/menAfriVacVisualizationReducer';
import mapReducer from '../shared/reducers/mapReducer';
import mediaItemsReducer from '../shared/reducers/mediaItemsReducer';

import caseStudyService from '../services/caseStudyService';
import miniVisualizationService from '../services/miniVisualizationService';
import mediaItemService from '../services/mediaItemService';
import fetchNeededData from '../shared/lib/needsFetcher';

import keystone from 'keystone';

var importRoutes = keystone.importer(__dirname);

import {getTemplateCache, setTemplateCache} from './templateCache';
import renderView from './renderView';

// Setup Route Bindings
export default function(server) {

  Fetcher.registerService(caseStudyService);
  Fetcher.registerService(miniVisualizationService);
  Fetcher.registerService(mediaItemService);
  server.use('/api', Fetcher.middleware());

  // Secure site during development — Remove this for production...
  //server.use((req, res, next) => {
  //  if(!req.user) {
  //    req.flash('error', 'Please sign in to access this page.');
  //    res.redirect('/keystone/signin');
  //  }
  //  else {
  //    next();
  //  }
  //});

  server.use((req, res) => {

    var template = getTemplateCache(req.path);

    if(template) {
      res.send(template.html);
    }
    else {
      let location = createLocation(req.url)
      const store = applyMiddleware(thunk)(createStore)(combineReducers({caseStudiesReducer, kenyaVisualizationReducer, menAfriVacVisualizationReducer, mapReducer, mediaItemsReducer}));

      var fetcher = new Fetcher({
        xhrPath: '/api',
        req: req
      });

      match({ routes, location }, (error, redirectLocation, renderProps) => {
        if (redirectLocation) {
          res.redirect(301, redirectLocation.pathname + redirectLocation.search);
        }
        else if (error) {
          res.send(500, error.message);
        }
        else if (renderProps == null) {
          res.send(404, 'Not found');
        }
        else {
          fetchNeededData(store.dispatch, renderProps.components, renderProps.params, fetcher)
            .then(() => {

              var renderedView = renderView(renderProps, req, store);

              setTemplateCache(req.path, renderedView);

              res.send(renderedView);
            })
            .catch((err) => {
              res.send(err.message);
            });
        }
      });
    } // else ends here...

  });

}
