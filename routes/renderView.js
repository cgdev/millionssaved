import React    from 'react';
import ReactDOM from 'react-dom/server';

import { Provider }    from 'react-redux';
import { RoutingContext } from 'react-router';

import prepareSocialMedia from './socialMeta';

export default function renderView(renderProps, req, store) {
  const InitialView = (
    <Provider store={store}>
      {() =>
        <RoutingContext {...renderProps} />
      }
    </Provider>
  );

  const componentHTML = ReactDOM.renderToString(InitialView);
  const initialState = store.getState();

  var scriptPath = '/js/client.js';
  var title = 'Millions Saved';

  var socialMeta = prepareSocialMedia(renderProps.routes, req, initialState);

  // <meta property="og:type" content="video.movie" />

  const HTML = `
    <!DOCTYPE html>
    <html>
      <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <meta property="og:title" content="${socialMeta.title}" />
        <meta property="og:description" content="${socialMeta.description}" />
        <meta property="og:url" content="${socialMeta.url}" />
        <meta property="og:image" content="${socialMeta.image}" />

        <meta name="twitter:card" content="summary_large_image" />
        <meta name="twitter:site" content="@CGDev" />
        <meta name="twitter:creator" content="@CGDev" />
        <meta name="twitter:title" content="${socialMeta.title}" />
        <meta name="twitter:description" content="${socialMeta.description}" />
        <meta name="twitter:image" content="${socialMeta.image}" />

        <title>${socialMeta.title}</title>
        <meta name="description" content="${socialMeta.description}" />
        <link rel="stylesheet" href="/styles/main.css">
      </head>
      <body id="body">
        <div id="view-wrapper" class="view-wrapper">
          <div id="react-view">${componentHTML}</div>
        </div>
        <div id="transition-overlay" class="transition-overlay"><div id="loader" class="loader"><div class="cube"></div><p>Loading</p></div></div>
        <script>
          window.__INITIAL_STATE__ = ${JSON.stringify(initialState)};
        </script>
        <script src="/js/lib/vendor.js" async></script>
        <script src="${scriptPath}" async></script>
      </body>
    </html>`;

  return HTML;
}
