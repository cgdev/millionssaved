
export default function prepareSocialMedia(routes, req, initialState) {

  var item = routes.pop();
  var title = item.socialMediaTitle;
  var description = 'Millions Saved is a collection of success stories in global health—remarkable cases in which large-scale efforts to improve health in developing countries have succeeded.';

  var pathname = req.url || req.pathname;
  var image = 'http://millionssaved.cgdev.org/img/about/ms-cover.jpg';
  var imgNameArray = [];

  if(title === 'Case Study') {
    title = initialState.caseStudiesReducer.fullCaseStudies[0].title;
    description = initialState.caseStudiesReducer.fullCaseStudies[0].intro;
    imgNameArray = initialState.caseStudiesReducer.fullCaseStudies[0].image && initialState.caseStudiesReducer.fullCaseStudies[0].image.filename ? initialState.caseStudiesReducer.fullCaseStudies[0].image.filename.split('.jpg') : [];
    image = imgNameArray && imgNameArray[0] ? `http://millionssaved.s3-us-west-2.amazonaws.com/md/${imgNameArray[0]}_md.jpg` : 'http://millionssaved.cgdev.org/img/case-studies/medium/case-study-' + initialState.caseStudiesReducer.fullCaseStudies[0].template + '.jpg';
  }

  return {
    title: title,
    description: description,
    url: 'http://millionssaved.cgdev.org' + (pathname === '/' ? '' : pathname),
    image: image
  };
}
