
var templateCache = {};

function getTemplateCache(templateName) {
  return templateCache[templateName];
}

function setTemplateCache(templateName, template) {
  templateCache[templateName] = {
    // valid: true,
    html: template
  };
}

function invalidateTemplates() {
  // Object
  //   .keys(templateCache)
  //   .forEach(function(key) {
  //     templateCache[key].valid = false;
  //   });
  
  // var keys = Object.keys(templateCache),
  //     len = keys.length;
  // for(var i = 0; i < len; i++) {
  //   templateCache[keys[i]].valid = false;
  // }

  templateCache = {};
}

export default {
  getTemplateCache: getTemplateCache,
  setTemplateCache: setTemplateCache,
  invalidateTemplates: invalidateTemplates
};
