
import keystone from 'keystone';

var CaseStudy = keystone.list('CaseStudy');

const caseStudyService = {
	name: 'case-study-service',
	read(req, resource, params, config, callback) {

		if(params.slug) {
			// console.time('single case fetcher');
			CaseStudy
				.model
				.findOne({ slug: params.slug, state: 'published' })
				.select(`
					-summary.approach.md
					-summary.impact.md
					-summary.cost.md
					-background.visualizationOne
					-background.visualizationTwo
					-background.visualizationThree
					-background.visualizationFour
					-background.visualizationFive
					-background.visualizationSix
					-programRollout.visualizationOne
					-programRollout.visualizationTwo
					-programRollout.visualizationThree
					-programRollout.visualizationFour
					-programRollout.visualizationFive
					-programRollout.visualizationSix
					-programRollout.visualizationSeven
					-programRollout.visualizationEight
					-programRollout.visualizationNine
					-impact.visualizationOne
					-impact.visualizationTwo
					-impact.visualizationThree
					-impact.visualizationFour
					-impact.visualizationFive
					-impact.visualizationSix
					-cost.visualizationOne
					-cost.visualizationTwo
					-cost.visualizationThree
					-cost.visualizationFour
					-cost.visualizationFive
					-cost.visualizationSix
					-reasonsForSuccess.visualizationOne
					-reasonsForSuccess.visualizationTwo
					-reasonsForSuccess.visualizationThree
					-reasonsForSuccess.visualizationFour
					-reasonsForSuccess.visualizationFive
					-reasonsForSuccess.visualizationSix
					-implications.visualizationOne
					-implications.visualizationTwo
					-implications.visualizationThree
					-implications.visualizationFour
					-implications.visualizationFive
					-implications.visualizationSix
				`)
				.exec()
				.then((caseStudy) => {
					// console.timeEnd('single case fetcher');
					callback(null, caseStudy);
				}, (err) => {
					// callback(err, [{ id: 404, title: 'Error.' }]);
					callback(err);
				});
		}
		else {
			// console.time('multi case fetcher');
			CaseStudy
				.model
				.find({ state: 'published' })
				.select('preTitle title slug caseType template location region approach image')
				.exec()
				.then((caseStudies) => {
					// console.timeEnd('multi case fetcher');
					callback(null, caseStudies);
				}, (err) => {
					// callback(err, [{ id: 404, title: 'Error.' }]);
					callback(err);
				});
		}

	}
}

export default caseStudyService;
