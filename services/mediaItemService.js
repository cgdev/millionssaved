
import keystone from 'keystone';

var MediaItem = keystone.list('MediaItem');

const mediaItemService = {
  name: 'media-item-service',
  read(req, resource, params, config, callback) {
    MediaItem
      .model
      .find({ state: 'published' })
      .exec()
      .then((mediaItems) => {
        callback(null, mediaItems);
      }, (err) => {
        callback(err);
      });
  }
}

export default mediaItemService;
