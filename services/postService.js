
import keystone from 'keystone';

var Post = keystone.list('Post');

// var posts = [
// 	{ id: 111, title: 'Something awesome', body: 'Nothing here.' },
// 	{ id: 112, title: 'Another post', body: 'Still nothing to say.' },
// 	{ id: 113, title: 'One Final Post', body: 'The architecture of this site is interesting.' }
// ];

const postService = {
	name: 'post-service',
	read(req, resource, params, config, callback) {
		
		Post
			.model
			.find({ state: 'published' })
			.exec()
			.then((posts) => {
				callback(null, posts);
			}, (err) => {
				callback(null, [{ id: 404, title: 'Error.' }]);
			});

		// setTimeout(function() {
		// 	callback(null, posts);
		// }, 200);

	}
}

export default postService;
