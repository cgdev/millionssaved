
import Fetcher from 'fetchr';
import findWhere from 'lodash/collection/findWhere';

export const REQUEST_CASE_STUDIES = 'REQUEST_CASE_STUDIES';
export const RECEIVE_CASE_STUDIES = 'RECEIVE_CASE_STUDIES';

export const REQUEST_CASE_STUDY = 'REQUEST_CASE_STUDY';
export const RECEIVE_CASE_STUDY = 'RECEIVE_CASE_STUDY';

export const CHANGE_LIST_STYLE = 'CHANGE_LIST_STYLE';
export const CHANGE_REGION_FILTER = 'CHANGE_REGION_FILTER';
export const CHANGE_APPROACH_FILTER = 'CHANGE_APPROACH_FILTER';

//
// Multiple Case Studies
//
function requestCaseStudies() {
  return {
    type: REQUEST_CASE_STUDIES
  };
}

function receiveCaseStudies(caseStudies) {
  return {
    type: RECEIVE_CASE_STUDIES,
    caseStudies: caseStudies,
    receivedAt: Date.now()
  };
}


//
// Single Case Studies
//
function requestCaseStudy() {
  return {
    type: REQUEST_CASE_STUDY
  };
}

function receiveCaseStudy(caseStudy) {
  return {
    type: RECEIVE_CASE_STUDY,
    caseStudy: caseStudy,
    receivedAt: Date.now()
  };
}


//
// Fetch Multiple Case Studies
//
function fetchCaseStudies(fetcher) {
  return (dispatch) => {
    dispatch(requestCaseStudies());

    var promise = new Promise((resolve, reject) => {

      fetcher
        .read('case-study-service')
        .end((err, data) => {

          if(err) {
            console.log('Rejecting case studies!');
            reject(err);
          }

          else {
            dispatch(receiveCaseStudies(data));
            resolve(data);
          }

        });

    });

    return promise;
  }
}


//
// Fetch Single Case Study
//
function fetchCaseStudy(fetcher, params) {
  return (dispatch) => {
    dispatch(requestCaseStudy());

    var promise = new Promise((resolve, reject) => {

      fetcher
        .read('case-study-service')
        .params({ slug: params.caseId })
        .end((err, data) => {
          if(err) {
            console.log('Rejecting case study!');
            reject(err);
          }

          else {
            dispatch(receiveCaseStudy(data));
            resolve(data);
          }

        });

    });

    return promise;
  }
}


//
// Check whether to fetch case studies
//
export function fetchCaseStudiesIfNeeded(params, fetcher) {
  return (dispatch, getState) => {
    if (shouldFetchCaseStudies(getState())) {
      return dispatch(fetchCaseStudies(fetcher));
    }
  };
}

function shouldFetchCaseStudies(state) {
  const caseStudies = state.caseStudiesReducer.caseStudies;
  if(state.caseStudiesReducer.isFetching) {
    return false;
  } else if(caseStudies.length === 0) {
    return true;
  } else {
    // return caseStudies.didInvalidate;
    return false;
  }
}


//
// Check whether to fetch singular case study
//
export function fetchCaseStudyIfNeeded(params, fetcher) {
  return (dispatch, getState) => {
    if (shouldFetchCaseStudy(getState(), params)) {
      return dispatch(fetchCaseStudy(fetcher, params));
    }
  };
}

function shouldFetchCaseStudy(state, params) {
  
  const caseStudies = state.caseStudiesReducer.fullCaseStudies;
  // console.log(caseStudies);
  if(state.caseStudiesReducer.isFetchingSingle) {
    // console.log('Already fetching case study.');
    return false;
  }
  
  if (caseStudies.length === 0) {
    // console.log('There are no case studies.');
    return true;
  }

  var targetCase = findWhere(caseStudies, { slug: params.caseId });

  if(targetCase) {
    // console.log('Found the case study.');
    return false;
  }
  else {
    // console.log('Did not find this case study.');
    return true;
  }

}

export function changeListStyle(listStyle) {
  return {
    type: CHANGE_LIST_STYLE,
    listStyle: listStyle
  };
}

export function changeRegionFilter(region) {
  return {
    type: CHANGE_REGION_FILTER,
    regionFilter: region
  };
}

export function changeApproachFilter(approach) {
  return {
    type: CHANGE_APPROACH_FILTER,
    approachFilter: approach
  };
}
