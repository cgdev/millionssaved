
export const KENYA_CHANGE_YEAR = 'KENYA_CHANGE_YEAR';
export const KENYA_CHANGE_DATASET = 'KENYA_CHANGE_DATASET';

export function changeYear(newYear) {
  return {
    type: KENYA_CHANGE_YEAR,
    newYear: newYear
  };
}

export function changeDataset(newDataset) {
  return {
    type: KENYA_CHANGE_DATASET,
    newDataset: newDataset
  };
}

