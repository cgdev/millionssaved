
import Fetcher from 'fetchr';
import findWhere from 'lodash/collection/findWhere';

import mediaItemsReducer from '../reducers/mediaItemsReducer';

export const REQUEST_MEDIA_ITEMS = 'REQUEST_MEDIA_ITEMS';
export const RECEIVE_MEDIA_ITEMS = 'RECEIVE_MEDIA_ITEMS';

//
// Multiple Media Items
//
function requestMediaItems() {
  return {
    type: REQUEST_MEDIA_ITEMS
  };
}

function receiveMediaItems(mediaItems) {
  return {
    type: RECEIVE_MEDIA_ITEMS,
    mediaItems: mediaItems,
    receivedAt: Date.now()
  };
}

//
// Fetch Multiple Media Items
//
function fetchMediaItems(fetcher) {
  return (dispatch) => {
    dispatch(requestMediaItems());

    var promise = new Promise((resolve, reject) => {

      fetcher
        .read('media-item-service')
        .end((err, data) => {

          if(err) {
            console.log('Rejecting media items!');
            reject(err);
          }

          else {
            dispatch(receiveMediaItems(data));
            resolve(data);
          }

        });

    });

    return promise;
  }
}

//
// Check whether to fetch Media Items
//
export function fetchMediaItemsIfNeeded(params, fetcher) {
  return (dispatch, getState) => {
    if (shouldFetchMediaItems(getState())) {
      return dispatch(fetchMediaItems(fetcher));
    }
  };
}

function shouldFetchMediaItems(state) {
  const mediaItems = state.mediaItemsReducer.mediaItems;
  if(state.mediaItemsReducer.isFetching) {
    return false;
  } else if(mediaItems.length === 0) {
    return true;
  } else {
    // return mediaItems.didInvalidate;
    return false;
  }
}
