
export const MENAFRIVAC_CHANGE_YEAR = 'MENAFRIVAC_CHANGE_YEAR';
export const MENAFRIVAC_CHANGE_DATASET = 'MENAFRIVAC_CHANGE_DATASET';

export function changeYear(newYear) {
  return {
    type: MENAFRIVAC_CHANGE_YEAR,
    newYear: newYear
  };
}

export function changeDataset(newDataset) {
  return {
    type: MENAFRIVAC_CHANGE_DATASET,
    newDataset: newDataset
  };
}
