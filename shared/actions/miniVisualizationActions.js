
import Fetcher from 'fetchr';
import findWhere from 'lodash/collection/findWhere';

export const REQUEST_MINI_VISUALIZATIONS = 'REQUEST_MINI_VISUALIZATIONS';
export const RECEIVE_MINI_VISUALIZATIONS = 'RECEIVE_MINI_VISUALIZATIONS';
// export const REQUEST_MINI_VISUALIZATION = 'REQUEST_MINI_VISUALIZATION';
// export const RECEIVE_MINI_VISUALIZATION = 'RECEIVE_MINI_VISUALIZATION';

function requestMiniVisualizations() {
  return {
    type: REQUEST_MINI_VISUALIZATIONS
  };
}

function receiveMiniVisualizations(visualizations) {
  return {
    type: RECEIVE_MINI_VISUALIZATIONS,
    visualizations: visualizations,
    receivedAt: Date.now()
  };
}

// function requestMiniVisualization() {
//   return {
//     type: REQUEST_MINI_VISUALIZATIONS
//   };
// }
//
// function receiveMiniVisualization(visualization) {
//   return {
//     type: RECEIVE_MINI_VISUALIZATION,
//     visualization: visualization,
//     receivedAt: Date.now()
//   };
// }

function fetchMiniVisualizations(fetcher, params) {
  return (dispatch) => {
    dispatch(requestMiniVisualizations());

    var promise = new Promise((resolve, reject) => {
      fetcher
        .read('mini-visualization-service')
        .params({ slug: params.caseId })
        .end((err, data) => {
          if(err) {
            reject(err);
          }

          else {
            dispatch(receiveMiniVisualizations(data));
            resolve(data);
          }
        });

    });

    return promise;
  }
}

export function fetchMiniVisualizationsIfNeeded(params, fetcher) {
  return (dispatch, getState) => {
    if(shouldFetchMiniVisualizations(getState(), params)) {
      return dispatch(fetchMiniVisualizations(fetcher, params));
    }
  };
}

function shouldFetchMiniVisualizations(state, params) {
  const caseStudies = state.caseStudiesReducer.fullCaseStudies;
  const targetCase = findWhere(caseStudies, { slug: params.caseId });

  return !targetCase.visualizationsLoaded;
}
