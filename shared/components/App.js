
import React from 'react';
import { Link } from 'react-router';
import Navigation from './Navigation';

import checkForNestedNeeds from '../lib/nestedNeedsChecker';

class App extends React.Component {
	// componentWillReceiveProps() {}
	// componentDidUpdate() {}
	// componentDidMount() {}
	constructor(props) {
		super(props);
		
		this.state = {
			navigationActive: false,
			navigationSticky: false,
			hideSiteNavigation: false
		};

		this.toggleNavigation = this.toggleNavigation.bind(this);
		this.forceCloseNavigation = this.forceCloseNavigation.bind(this);
	}
	componentDidMount() {
		var hideSiteNavigation = window.location.pathname == '/embeddable-map';

		if(hideSiteNavigation) {
			this.setState({hideSiteNavigation: true});
		}

		var scrollY = window.scrollY || window.pageYOffset;
		this.setState({navigationSticky: scrollY > 0});

		window.addEventListener('resize', () => {
			this.forceCloseNavigation();
		});

		var addSticky = () => {
			var scrollY = window.scrollY || window.pageYOffset;
			if(scrollY > 0) {
				this.setState({navigationSticky:true});
				window.removeEventListener('scroll', addSticky);
				window.addEventListener('scroll', removeSticky);
			}
		}

		var removeSticky = () => {
			var scrollY = window.scrollY || window.pageYOffset;
			if(scrollY < 1) {
				this.setState({navigationSticky:false});
				window.removeEventListener('scroll', removeSticky);
				window.addEventListener('scroll', addSticky);
			}
		}

		window.addEventListener('scroll', addSticky);

	}
	forceCloseNavigation() {
		this.setState({
			navigationActive: false
		});
	}
	toggleNavigation() {
		this.setState({
			navigationActive: !this.state.navigationActive
		});
	}
	render() {

		var wrapperStyles = {
			height: this.state.navigationActive ? window.innerHeight : 'auto',
			overflow: this.state.navigationActive ? 'hidden' : 'visible'
		};

		return (
			<div style={wrapperStyles}>
				<header className={this.state.navigationSticky ? 'site-header site-header--sticky' : 'site-header'} style={{display: this.state.hideSiteNavigation ? 'none' : 'block'}}>
					<div className={this.state.navigationActive ? 'container px-xs-1' : 'container px-xs-1'}>
						<div className='nav-wrapper clearfix'>
							<div className='logo'>
								<a href='//www.cgdev.org/' target='_blank'><img className='cgd-logo' src='/img/cgd-logo.png' height='61' alt='Center for Global Development'/></a>
								<h1 className='ms-logo'><Link to='/'>Millions<br /><span className='text--yellow'>Saved</span></Link></h1>
							</div>
							<Navigation toggleNavigation={this.toggleNavigation} navigationActive={this.state.navigationActive} forceCloseNavigation={this.forceCloseNavigation} />
							<div className='get-book visible--md'>
								<a href='//www.amazon.com/Millions-Saved-Proven-Success-Global/dp/1933286881' target='_blank' className='btn bg--yellow' style={{float: 'right'}}>Get the Book</a>
							</div>
						</div>
					</div>
				</header>

				<section className={this.state.hideSiteNavigation ? 'wrapper-section' : 'wrapper-section wrapper-section--width-shadow'}>
					{this.props.children}
				</section>

				<footer className='bg--grey' style={{display: this.state.hideSiteNavigation ? 'none' : 'block'}}>
					<div className='container px1 pt4 pb3'>
						<div className='mxn1 clearfix'>

							<div className='col col-md-3 px1'>
								<p>For more information about the Center for Global Development, visit <a href='//www.cgdev.org.' target='_blank'>CGDEV.org</a>.</p>
								<p>Contact us: <a href='mailto:globalhealthpolicy@cgdev.org?Subject=Millions%20Saved' target='_top'>globalhealthpolicy@cgdev.org</a></p>
							</div>

							<div className='col col-md-3 px1'>
								<p>Sign up for CGD’s Global Health Policy newsletter</p>
								<a href='//www.cgdev.org/page/global-health-newsletter' target='_blank' className='btn bg--yellow py05 px1'>Sign up</a>
							</div>

							<div className='col col-md-3 px1'>
								<p>Quick links</p>
								<ul className='m0 p0 li--unstyled'>
									<li><Link to='/about'>About</Link></li>
									<li><Link to='/findings'>Findings</Link></li>
									<li><Link to='/frequently-asked-questions'>FAQ</Link></li>
								</ul>
							</div>

							<div className='col col-md-3 px1'>
								<p>Engage with us</p>
								<ul className='m0 p0 li--unstyled'>
									<li><a href='//twitter.com/CGDev' target='_blank'>Twitter</a></li>
									<li><a href='//facebook.com/CGDev' target='_blank'>Facebook</a></li>
								</ul>
							</div>

							<div className='col col-md-12 px1 pt3 text--center'>
								<p className='text--muted m0'>&copy; 2015 — Center for Global Development</p>
							</div>

						</div>
					</div>
				</footer>

				<div className={this.state.navigationActive ? 'nav-cover nav-cover--active' : 'nav-cover'} onClick={this.forceCloseNavigation}></div>

			</div>
		);
	}
}

App.needs = checkForNestedNeeds(Navigation);

export default App;
