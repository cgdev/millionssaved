
import React from 'react';
import { connect } from 'react-redux';
import { fetchCaseStudyIfNeeded } from '../actions/caseStudyActions';
import { fetchMiniVisualizationsIfNeeded } from '../actions/miniVisualizationActions';

import findWhere from 'lodash/collection/findWhere';

// import CaseStudyCover from './caseStudies/utilities/CaseStudyCover';
import CaseStudySummary from './caseStudies/utilities/CaseStudySummary';

import BasicCaseTemplate from './caseStudies/BasicCaseTemplate';
import Cases from './caseStudies';
import RelatedCases from './caseStudies/utilities/RelatedCases';

class CaseStudy extends React.Component {
	constructor() {
		super();

		this.loadMiniVisualizations = this.loadMiniVisualizations.bind(this);
	}

	loadMiniVisualizations(caseStudy) {
		this.props.dispatch(fetchMiniVisualizationsIfNeeded({caseId: caseStudy.slug}, window.fetcher))
				.then(() => {
					this.forceUpdate();
				});
	}

	render() {
		var caseStudy = findWhere(this.props.caseStudies, { slug: this.props.params.caseId });
		var CaseStudyContent = caseStudy.caseType === 1 ? Cases[caseStudy.template] : BasicCaseTemplate;

		return (
			<article className='case-study'>

				<CaseStudyContent caseStudy={caseStudy} loadMiniVisualizations={this.loadMiniVisualizations} caseId={this.props.params.caseId} />

				<RelatedCases caseId={this.props.params.caseId} caseStudiesListing={this.props.caseStudiesListing} />

			</article>
		);
	}
}

CaseStudy.needs = [ fetchCaseStudyIfNeeded ];

function mapStateToProps(state) {
	return {
		activeIndex: state.caseStudiesReducer.activeIndex,
		caseStudiesListing: state.caseStudiesReducer.caseStudies,
		caseStudies: state.caseStudiesReducer.fullCaseStudies,
		dispatch: state.caseStudiesReducer.dispatch
	};
}

export default connect(mapStateToProps)(CaseStudy);
