
import React from 'react';
import { connect } from 'react-redux';
import OverviewMap from './OverviewMap';
import { fetchCaseStudiesIfNeeded } from '../actions/caseStudyActions';

class EmbeddableMap extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      standalone: false
    };
  }
  componentDidMount() {
    this.setState({ standalone: window.location.pathname == '/embeddable-map' });
  }
  render() {
    return (
      <div>
        <OverviewMap caseStudies={this.props.caseStudies}
                     approachFilter={this.props.approachFilter}
                     regionFilter={this.props.regionFilter}
                     standalone={this.state.standalone} />
      </div>
    );
  }
}

EmbeddableMap.needs = [fetchCaseStudiesIfNeeded];

function mapStateToProps(state) {
  return {
    caseStudies: state.caseStudiesReducer.caseStudies,
    regionFilter: state.caseStudiesReducer.regionFilter,
    approachFilter: state.caseStudiesReducer.approachFilter,
    dispatch: state.caseStudiesReducer.dispatch
  };
}

export default connect(mapStateToProps)(EmbeddableMap);