
import React from 'react';
import { Link } from 'react-router';

import GetBook from './caseStudies/utilities/GetBook';
import Collapsible from './caseStudies/utilities/Collapsible';

class FAQ extends React.Component {
	componentDidMount() {
		document.title = 'Millions Saved — FAQ';
	}
	render() {
		return (
			<div>
				<header className='banner banner--page'>
					<img src='img/about/faq-cover.jpg' />
					<div className='banner-overlay text--center'>
						<div className='container--narrow y-center clearfix px1 px-sm-2'>

							<h1 className='element--0'>
								<span className='subhead text--yellow text--uppercase'>Millions Saved</span><br /><hr className='divider--sm' />
								<span className='display--4'>FAQ</span>
							</h1>
							<p className='subhead visible--sm element--1 text--yellow text--uppercase'>Everything You Need To Know</p>

						</div>
					</div>
					<div className='img-source'>
						<a href='//www.flickr.com/photos/cgdev/' target='_blank'>Photo credit: CGDEV</a>
					</div>
				</header>

				<section className='bg--grey pb3'>

					<div className='container--narrow text--center px1 px-sm-2 py3'>
						<p className='lead'>Here are some of the most common questions we receive about Millions Saved — from how the project got started to how we measured the success of the programs covered in the book. If your question is not answered below, please <a href="mailto:globalhealthpolicy@cgdev.org">contact us</a>.</p>
					</div>

					<Collapsible sign='1' title='What is Millions Saved? How did it get started?'>
						<p>Millions Saved is an initiative of the Center for Global Development (CGD), a “think and do tank” with headquarters in Washington, DC. In 2004, CGD convened global health and development experts as part of the What Works Working Group, chaired by Ruth Levine, to determine what works in global health. The result of the working group was Millions Saved: Proven Successes in Global Health—a book featuring 17 large-scale efforts to improve health in developing countries that succeeded. Together with the two subsequent editions of Millions Saved (published in 2007 and 2016), these case studies show that large-scale success in health is possible—countering a common view that health problems in the developing world are intractable, and that development assistance for health yields few benefits.</p>
					</Collapsible>

					<Collapsible sign='2' title='What is the difference between the various editions of Millions Saved?'>
						<p>The original Millions Saved, published by the Center for Global Development in 2004, described 17 large-scale global health successes. In 2007, an updated and expanded edition was published through Jones and Bartlett, featuring the original 17 cases plus three new chapters. The 2016 edition profiles 22 new cases: 18 major achievements and 4 examples of promising interventions that could not demonstrate improved health when scaled up in real world conditions. The newest edition also introduced additional selection criteria for its cases; preference was given to programs that could show cost-effectiveness in implementation, global relevance, and/or improvements in equity or financial protection.</p>
					</Collapsible>

					<Collapsible sign='3' title='How did you choose the programs to include in the book?'>
						<p>The Millions Saved case selection process was rigorous, open and transparent. Programs were selected based on four key criteria developed by the original What Works Working Group: importance, impact, scale, and duration. These criteria were updated for the latest version, giving preference to programs that could also show cost-effectiveness in implementation, global relevance, and/or improvements in equity. (For more details, read the <a href="/about/cases">Selection Criteria</a>.)</p>
						<p>For the 2016 edition, we partnered with the editors of the Disease Control Priorities, 3rd Edition, to find cases and studies; issued public calls for submissions; conducted interviews with subject matter experts; and searched the literature. We relied on an external expert advisory group and an internal advisory group of economists to consider a list of cases that met at least some of the Millions Saved criteria. Through this process, we generated a short-list of cases included in the book.</p>
					</Collapsible>

					<Collapsible sign='4' title='How do you know these programs worked?'>
						<p>In each success story, solid evidence—summarized in the “Strength of the Evidence” box—strongly suggests that health improvements are attributable to the specific program or policy of interest rather than other factors, such as economic growth or social improvements. The cases are based on documented evidence from scientific articles published in peer-reviewed journals and/or rigorous impact evaluations of the programs, as well as interviews with key informants. Each case was reviewed by technical experts knowledgeable about the program.</p>
						<p>However, the cases are not without controversy. This is particularly true for three featured programs: Kenya’s school-based deworming, India’s Avahan HIV control, and Indonesia’s reduction of open defecation. For each case, the chapter describes the reasons for disagreement—conflicting studies, weaknesses in the evidence base, or debates about appropriate tactics, for example—allowing the reader to follow and critically assess the debate. These disagreements offer important lessons for global health policymakers and underscore the importance of rigorous impact evaluation, local context, and systematic reviews of all available evidence.</p>
					</Collapsible>

					<Collapsible sign='5' title='Are you saying these programs are the best ones, the ones we should be putting our money into?'>
						<p>Instead of suggesting a list of specific programs that should be funded in the future, Millions Saved offers guidance on core ingredients for increasing the likelihood that global health programs of all types will succeed. These include predictable, adequate funding; political leadership; technical consensus; good management; sensitive monitoring and evaluation; and affordable technical innovations within an effective delivery system.</p>
					</Collapsible>

					<Collapsible sign='6' title='Who funded the programs included in Millions Saved?'>
						<p>In most programs, funding was provided through the government’s own coffers and supported by a donor—including multilateral and bilateral institutions, private donors, and NGOs such as UNICEF, the Bill and Melinda Gates Foundation, Gavi, the World Bank, Merck Foundation and the UK’s Department for International Development—just to name a few.</p>
					</Collapsible>

					<Collapsible sign='7' title='Are these programs sustainable?'>
						<p>Financial sustainability, or the program’s ability to continue once donor funding ends, was not a criterion included in the book. However, each program lasted at least five years, and many continue today.</p>
					</Collapsible>

					<Collapsible sign='8' title='Where have the programs featured in Millions Saved been implemented?'>
						<p>Programs included in Millions Saved have been implemented in all regions of the world. Programs took place in Asia and South Asia, including in China, Thailand, Nepal, Sri Lanka, Bangladesh, India, Indonesia, Vietnam and Pakistan; the Americas, including Brazil, Mexico, Chile, Argentina, and Peru; and Africa, including South Africa, Rwanda, Kenya, Egypt, Botswana, Morocco and The Gambia. A few case studies cover programs in the Caribbean, including in Haiti and Jamaica, and one case in the first edition featured a European program in Poland.</p>
					</Collapsible>

					<Collapsible sign='9' title='These programs sound great, but would they work in a country with an unstable government or in a fragile state?'>
						<p>Many of the programs identified in Millions Saved benefited from stable political environments, but progress has been made even where the public sector was very weak. For instance, despite their relatively unstable political environments, Haiti eliminated polio following a resurgence and Pakistan increased girls’ access to school, enabling more young women to delay marriage through a socially acceptable path.</p>
					</Collapsible>

					<Collapsible sign='10' title='Do all Million Saved programs tackle a single disease or affliction? What about health systems?'>
						<p>Millions Saved documents many different types of programs, including several that focus on a specific disease or intervention through a stand-alone architecture separate from the routine health system—often called “vertical” programs. Other types of programs have also succeeded, including overall health system strengthening and legal or regulatory improvements. Several of these are included in Millions Saved.</p>
					</Collapsible>

					<Collapsible sign='11' title='Do the successful programs in Millions Saved share certain core characteristics?'>
						<p>Although each case is unique and context-specific, we found all of the cases in Millions Saved had four features in common: interventions were evidence-based, strategic partnerships were established, efforts were sustained by political will, and monitoring and evaluation were made a priority. There are were also seven key lessons learned through all the cases. Check out our <Link to="/findings">Findings page</Link> to learn more.</p>
					</Collapsible>

					<Collapsible sign='12' title='The problems facing the world now (e.g., greater burden of non-communicable diseases) are different than the problems in the past. Does any of this really apply to the challenges of today and the future?'>
						<p>Much has changed in global health since the first edition of Millions Saved, but much remains the same. Global health priorities have shifted since the original Million Saved case studies were compiled, most obviously in the transition from the Millennium Development Goals (MDGs) to the Global Goals for Sustainable Development. Non-communicable diseases (NCDs) have risen in prominence on the global health agenda; the global health community has rallied behind “universal health coverage; results-based funding has proliferated around the globe; and health experts now give serious attention to social determinants that shape health outcomes. The cases selected for the latest edition reflect these major shifts. Still, the MDG agenda remains unfinished. Preventable maternal, infant, and child mortality, under-nutrition, and infectious diseases are still too common. Emerging drug resistance and the threat of malaria resurgence entreat us to remain vigilant, even where specific threats lay dormant. Ultimately, the lessons gleaned from the success in each case provide both inspiration and essential guidance for how major success against problems old and new can be achieved.</p>
					</Collapsible>

					<Collapsible sign='13' title='Is the content on the website the same as the content in the book?'>
						<p>The cases featured on the website are shortened versions of the respective book chapters. In the book, each case study category is prefaced by an introductory section along with headlines for each case, and how they fit with the “wows” highlighted in the introduction. The book begins with a preface, forward, and introduction, and ends with a chapter on methods.</p>
						<p>In the book, each case follows a similar storyline: setting out the facts of the policy or program at a glance; defining the health problem addressed and the approach undertaken; describing the health impact and the strength of the evidence; considering the cost of achieving that impact; summarizing the keys to lasting success; and analyzing the case’s implications for global health more broadly. The ten website cases include most sections, but in less detail.</p>
					</Collapsible>

					<Collapsible sign='14' title='Where can I find more information about the earlier editions of Millions Saved?'>
						<p>Summaries and full text of each of the 20 cases featured in the first editions of Millions Saved can be found on the <a href='http://www.cgdev.org/page/elements-success' target='_blank'>Center for Global Development website.</a></p>
					</Collapsible>

					<Collapsible sign='15' title='Who wrote Millions Saved?'>
						<p>The latest edition was coauthored by Amanda Glassman, Vice President for Programs and Director of Global Health Policy at the Center for Global Development, and Miriam Temin, Project Director, Building Girls’ HIV Prevention Assets for DREAMS, at the Population Council. The case chapters were originally authored by Alix Beith, Lauren Post, Yuna Sakuma, Rachel Silverman, and Miriam Temin. All chapters were reviewed by external experts who were familiar with each program.</p>
					</Collapsible>

					<Collapsible sign='16' title='Who funded the Millions Saved project, and did this impact which cases you chose?'>
						<p>The Center for Global Development is grateful for the financial support and technical feedback of the Bill & Melinda Gates Foundation and Good Ventures.</p>
						<p>Gates Foundation staff played a role in the book’s production by participating in the review of evidence around a short-list of cases put together by the CGD team and by providing advice and feedback on the overall project. However, the cases were selected independently and each chapter was written and edited by individuals with no involvement or direct interest in the included programs.</p>
						<p>CGD is an independent and nonpartisan research institution. There are no conditions or limitations on CGD’s independence in research, findings, conclusions, or resulting publications attached to any funding received. You can learn more about the <a href="http://www.cgdev.org/page/mission" target="_blank">CGD’s mission</a> and <a target="_blank" href="http://www.cgdev.org/section/funding"> funding policies</a>.</p>
					</Collapsible>

				</section>

				<section>
					<GetBook />
				</section>

			</div>
		);
	}
}

export default FAQ;
