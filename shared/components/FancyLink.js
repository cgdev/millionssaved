
import React from 'react';

export default class FancyLink extends React.Component {

  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(e) {
    e.preventDefault();
    
    // Trigger transition event > Trigger async request and transition start

    setTimeout(() => {
      this.context.router.transitionTo('/about');
    }, 1000);

    // Listen for response before transitioning to new view with the below
    // this.context.router.transitionTo('/about');
  }

  render() {
    return (<a href='/about' onClick={this.handleClick}>About</a>);
  }
}

FancyLink.contextTypes = {
  router: React.PropTypes.func.isRequired
};
