
import React from 'react';
import { Link } from 'react-router';

import { connect } from 'react-redux';
import {fetchMediaItemsIfNeeded} from '../actions/mediaItemActions';

// import minigrid from 'minigrid';

class Media extends React.Component {
  constructor(props) {
    super(props);
    this.grid = this.grid.bind(this);
  }
  grid() {
    var grid = new window.Minigrid({
      container: '.media-item-grid',
      item: '.media-item-grid-item',
      gutter: 0
    });
    grid.mount();
  }
  componentDidMount() {
    document.title = 'Millions Saved — Media';
    this.grid();
    window.addEventListener('resize', this.grid);
  }
  componentWillUnmount() {
    window.removeEventListener('resize', this.grid);
  }
  render() {
    return (
      <div>
        <header className='banner banner--page'>
          <img src='/img/about/media-cover.jpg' />
          <div className='banner-overlay text--center'>
            <div className='container--narrow y-center clearfix px1 px-sm-2'>
              <h1 className='element--0'>
                <span className='subhead text--yellow text--uppercase'>Millions Saved</span><br /><hr className='divider--sm' />
                <span className='display--4'>Media</span>
              </h1>
              <p className='subhead visible--sm element--1 text--yellow text--uppercase'>Millions Saved in the Press</p>
            </div>
          </div>
          <div className='img-source'>
            <a href='//www.flickr.com/photos/62693815@N03/6276688407/in/photolist-ayDEMD-hTvHsn-2Vrakq-8oHs4W-ayWQ4v-F4bK8-y8XfoL-dwyPFS-q8M1Fy-hTiNjv-4A9eBb-ayGksm-ikaRV-EbcaC-2r5Aw-4p4VfT-8kXM7-8kXFD-8kXL7-8kXKs-8kXMX-8kXGA-8kXEm-iSeKL-8kXDd-8kXJp-4LTEDT-8kXBN-8kXHF-8kXCA-7DSX3q-4Xuud3-dki4iG-k2EJXd-dSyb8k-48vQEC-aUskV-5W1aoU-9nSE1b-ccWpgA-hBXz-nwpEPB-iyEc2R-ayZtq9-7AUALs-hTPi8f-oeLuqj-nDYNs7-r24Tnj-5Pfj8r' target='_blank'>Photo credit: Jon S</a>
          </div>
        </header>

        <section className='bg--grey pb3'>

          <div className='container--narrow text--center px1 px-sm-2 py3'>
            <p className='lead'>
              This is a collection of news articles, podcasts, and blog posts about the programs featured in and lessons learned from Millions Saved. Here you can find content created as part of our partnership with The Guardian and more.
              <br/><br/>
                For media inquiries, contact Holly Shullman at <a href="mailto:hshulman@cgdev.org">hshulman@cgdev.org</a> (202) 416-4040.
            </p>
          </div>

          <div className='container--narrow'>
            <div className='media-item-grid col-sm-12 clearfix'>
              {this.props.mediaItems.map((item, i) => {
                return <MediaItem data={item} key={i} />;
              })}
            </div>
          </div>
        </section>

      </div>
    );
  }
}


class MediaItem extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {

    var monthNames = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    var publishedAt = new Date(this.props.data.publishedAt);
    var sourcePublished = new Date(this.props.data.source.publishedAt);

    return (
      <div className='media-item-grid-item col col-sm-6 px1 px-sm-2 py2'>
        <div className='bg--white px2 py2'>
          { /* <p>{ `${monthNames[publishedAt.getMonth()]} ${publishedAt.getDate()}, ${publishedAt.getFullYear()}` }</p> */ }

          <h2>{this.props.data.title}</h2>
          <p>{this.props.data.intro}</p>

          <p className='text--muted'>Published in <a href={this.props.data.source.url}>{this.props.data.source.name}</a> { `${monthNames[sourcePublished.getMonth()]} ${sourcePublished.getDate()}, ${sourcePublished.getFullYear()}` }</p>

          <a href={this.props.data.source.url} target='_blank' className='btn bg--yellow py05 px1'>Visit Source</a>

        </div>
      </div>
    );
  }
}

Media.needs = [ fetchMediaItemsIfNeeded ];

function mapStateToProps(state) {
  return {
    mediaItems: state.mediaItemsReducer.mediaItems,
    dispatch: state.mediaItemsReducer.dispatch
  };
}

export default connect(mapStateToProps)(Media);
