
import React from 'react';
import { connect } from 'react-redux';

import { Link } from 'react-router';
import Icon from '../lib/icons';

import colors from './caseStudies/utilities/colors';

import { fetchCaseStudiesIfNeeded } from '../actions/caseStudyActions';

class NavItem extends React.Component {
	render() {

		var {slug, name, itemStyle} = this.props;

		return (
			<li className={itemStyle}>
				<Link to={slug} activeClassName='active' onClick={this.props.forceCloseNavigation}>
					<span>{name}</span>
				</Link>
			</li>
		);
	}
}

class NavDropdown extends React.Component {
	constructor() {
		super();

		this.state = {
			open: false,
			mobileMenu: true
		};

		this.openDropdown = this.openDropdown.bind(this);
		this.closeDropdown = this.closeDropdown.bind(this);
		this.catchClick = this.catchClick.bind(this);
		this.resizeNavigation = this.resizeNavigation.bind(this);
	}

	componentDidMount() {
		this.resizeNavigation();
		window.addEventListener('resize', this.resizeNavigation);
	}
	resizeNavigation() {
		if(window.innerWidth < 992 && !this.state.mobileMenu ) {
			this.setState({mobileMenu: true });
		}
		else if(window.innerWidth > 992 && this.state.mobileMenu) {
			this.setState({mobileMenu: false });
		}
	}

	catchClick(e) {
		// e.preventDefault();
		this.closeDropdown();
	}

	openDropdown() {
		this.setState({
			open: true
		});
	}

	closeDropdown() {
		this.props.forceCloseNavigation();
		this.setState({
			open: false
		});
	}

	render() {

		var {slug, name, dropdown, itemStyle} = this.props;

		var openDropdown = this.openDropdown;
		var closeDropdown = this.closeDropdown;
		var itemActiveStyle = {};

		return (
			<li className={itemStyle} style={itemActiveStyle}>

				<a href={`/${slug}`} activeClassName='active' onMouseEnter={this.state.mobileMenu ? null : openDropdown} onMouseLeave={this.state.mobileMenu ? null : closeDropdown} onFocus={openDropdown} onBlur={closeDropdown} onClick={this.catchClick}>
					<span>{name}</span>
				</a>

				<div className={this.state.open ? 'dropdown dropdown--open' : 'dropdown'} onMouseEnter={this.state.mobileMenu ? null : openDropdown} onMouseLeave={this.state.mobileMenu ? null : closeDropdown}>
					<div style={{background: colors.teal500}}>
						<div className='container px1'>
							<ul>
								{dropdown.map((item, key) => {
									if(item.caseType === 0) return;
									return (
										<li key={key} className='main-navigation__dropdown-item col col-md-4 px1 mb1'>
											<Link to={`/case-studies/${item.slug}`} className='dropdown__link' onFocus={openDropdown} onBlur={closeDropdown} onClick={closeDropdown}>
												{item.title}
											</Link>
										</li>
									);
								})}
							</ul>
						</div>
						<div className='container px2 pb2 visible--md text--center'>
							<a href={`/${slug}`} onFocus={openDropdown} onClick={closeDropdown} className='btn bg--yellow px2 py1'>See all case studies</a>
						</div> 
					</div>
				</div>

			</li>
		);
	}
}

class Navigation extends React.Component {
	render() {

		var itemStyle = 'main-navigation__item px1';

		var navItems = [
			{
				slug: '#case-studies',
				name: 'Case Studies',
				dropdown: this.props.caseStudies,
				itemStyle: itemStyle + ' main-navigation__item--with-dropdown'
			},
			{
				slug: '/findings',
				name: 'Key Findings',
				itemStyle: itemStyle
			},
			{
				slug: '/frequently-asked-questions',
				name: 'FAQ',
				itemStyle: itemStyle
			},
			{
				slug: '/media',
				name: 'Media',
				itemStyle: itemStyle
			},
			{
				slug: '/about',
				name: 'About',
				itemStyle: itemStyle
			}
		];

		return (
			<div className='main-navigation'>
				<div className='main-navigation__toggle'>
					<button className='btn bg--yellow px1 py1' onClick={this.props.toggleNavigation}>
						<Icon name='nav' color='#333333' /><span style={{marginLeft:'8px'}} className='visible--sm-inline'>MENU</span>
					</button>
				</div>
				<nav className={this.props.navigationActive ? 'nav nav--active' : 'nav'}>
					<ul>

						{navItems.map((navItem, key) => {

							if(!navItem.dropdown)
								return <NavItem {...navItem} toggleNavigation={this.props.toggleNavigation} forceCloseNavigation={this.props.forceCloseNavigation} key={key}/>;

							else
								return <NavDropdown {...navItem} toggleNavigation={this.props.toggleNavigation} forceCloseNavigation={this.props.forceCloseNavigation} key={key}/>;

						})}

					</ul>
				</nav>
			</div>
		);
	}
}

Navigation.needs = [ fetchCaseStudiesIfNeeded ];

function mapStateToProps(state) {
	return {
		caseStudies: state.caseStudiesReducer.caseStudies,
		dispatch: state.caseStudiesReducer.dispatch
	};
}

export default connect(mapStateToProps)(Navigation);
