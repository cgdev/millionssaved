
import React from 'react';
import { Link } from 'react-router';

class Team extends React.Component {
	componentDidMount() {
		document.title = 'About Millions Saved — The Team';
	}
	render() {
		return (
			<div>
				
				<div className='bg--grey'>
					<div className='container--narrow px1 px-sm-2 pt1 pb2 mb4'>

						<div className='mxn1 mx-sm-n2 clearfix text--center'>
							<div className='col col-md-12 px1 px-sm-2'>
								<h2>
									<span className='subhead text--yellow text--uppercase'>The Team</span><br /><hr className='divider--sm' />
									<span className='display--2'>Authors</span>
								</h2>
							</div>

							<div className='col col-sm-6 px1 px-sm-2 mb2'>
								<img src='/img/about/authors/amanda.jpg' className='block' />
								<div className='bg--white clearfix py2 px1 px-sm-2' style={{minHeight: '34.8rem'}}>
									<h4 className='mt0'>
										<hr className='divider--sm' />
										<span className='title'>Amanda Glassman</span>
									</h4>
									<p>is the vice president for programs, director of global health policy, and a senior fellow at the Center for Global Development. She has 20 years of experience working on health and social protection policy and programs in the developing world.</p>
								</div>
							</div>
							<div className='col col-sm-6 px1 px-sm-2 mb2'>
								<img src='/img/about/authors/miriam.jpg' className='block' />
								<div className='bg--white clearfix py2 px1 px-sm-2' style={{minHeight: '34.8rem'}}>
									<h4 className='mt0'>
										<hr className='divider--sm' />
										<span className='title'>Miriam Temin</span>
									</h4>
									<p>is director of the DREAMS Capacity Strengthening project at the Population Council. She was previously a consultant to the Center for Global Development. Temin has nearly 20 years of experience developing strategies and advising on policies for donors, UN agencies, think tanks, and NGOs in the United States, Africa, and Europe.</p>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div className='bg--white'>
					<div className='container px1 px-sm-2 mb4'>

						<div className='mxn1 mx-sm-n2 clearfix text--center'>
							<div className='col col-md-12 px1 px-sm-2'>
								<h2>
									<span className='subhead text--yellow text--uppercase'>The Team</span><br /><hr className='divider--sm' />
									<span className='display--2'>Contributors</span>
								</h2>
							</div>
						</div>

						<div className='mxn1 clearfix text--center'>
							<div className='col col-xs-6 col-sm-4 col-md-fifth px1 py2'>
								<div className='col-sm-10 col-md-12'>
									<img src='/img/about/authors/alix.jpg' />
								</div>
								<h4>
									<hr className='divider--sm' />
									<span className='title'>Alexandra Beith</span>
								</h4>
								<p style={{fontSize:'1.4rem'}}>is a resident scholar at the Center for Disease Dynamics, Economics & Policy. She is the original author of four cases in Millions Saved (2016).</p>
							</div>
							<div className='col col-xs-6 col-sm-4 col-md-fifth px1 py2'>
								<div className='col-sm-10 col-md-12'>
									<img src='/img/about/authors/andrew.jpg' />
								</div>
								<h4>
									<hr className='divider--sm' />
									<span className='title'>Andrew Mirelman</span>
								</h4>
								<p style={{fontSize:'1.4rem'}}>is a research fellow in Global Health Economics at University of York. He calculated economic evaluaion estimates for several cases and contributed to the methods chapter of Millions Saved (2016).</p>
							</div>
							<div className='col col-xs-6 col-sm-4 col-md-fifth px1 py2'>
								<div className='col-sm-10 col-md-12'>
									<img src='/img/about/authors/lauren.jpg' />
								</div>
								<h4>
									<hr className='divider--sm' />
									<span className='title'>Lauren Post</span>
								</h4>
								<p style={{fontSize:'1.4rem'}}>is a program associate at the Center for Global Development. She is the original author of two cases in Millions Saved (2016).</p>
							</div>

							<div className='col col-xs-6 col-sm-4 col-sm-offset-2 col-md-fifth col-md-offset-0 px1 py2'>
								<div className='col-sm-10 col-md-12'>
									<img src='/img/about/authors/yuna.jpg' />
								</div>
								<h4>
									<hr className='divider--sm' />
									<span className='title'>Yuna Sakuma</span>
								</h4>
								<p style={{fontSize:'1.4rem'}}>is a program associate at the Center for Global Development. She is the original author of two cases in Millions Saved (2016).</p>
							</div>
							<div className='col col-xs-6 col-xs-offset-3 col-sm-4 col-sm-offset-0 col-md-fifth px1 py2'>
								<div className='col-sm-10 col-md-12'>
									<img src='/img/about/authors/rachel.jpg' />
								</div>
								<h4>
									<hr className='divider--sm' />
									<span className='title'>Rachel Silverman</span>
								</h4>
								<p style={{fontSize:'1.4rem'}}>is a senior policy analyst at the Center for Global Development. She is the original author of seven cases in Millions Saved (2016).</p>
							</div>
						</div>

					</div>
				</div>

				<div className='bg--grey'>
					<div className='container px1 px-sm-2 pb4'>

						<div className='mxn1 mx-sm-n2 clearfix text--center'>
							<div className='col col-md-12 px1 px-sm-2'>
								<h2>
									<span className='subhead text--yellow text--uppercase'>The Team</span><br /><hr className='divider--sm' />
									<span className='display--2'>Advisory Group</span>
								</h2>
							</div>
						</div>

						<div className='clearfix'>

							<div className='col col-sm-10 col-sm-offset-1'>
								<div className='mxn1 mx-sm-n2 clearfix text--center'>

									<div className='col col-xs-6 col-sm-4 px1 px-sm-2 py2'>
										<div className='text--center'>
											<img src='/img/about/advisors/elie-hassenfeld.jpg' className='col-sm-10 col-md-9'/>
										</div>
										<h4>
											<hr className='divider--sm' />
											<span className='title'>Elie Hassenfeld</span>
										</h4>
										<p>GiveWell</p>
									</div>
									<div className='col col-xs-6 col-sm-4 px1 px-sm-2 py2'>
										<div className='text--center'>
											<img src='/img/about/advisors/priscilla-idele.jpg' className='col-sm-10 col-md-9'/>
										</div>
										<h4>
											<hr className='divider--sm' />
											<span className='title'>Priscilla Idele</span>
										</h4>
										<p>UNICEF</p>
									</div>
									<div className='col col-xs-6 col-sm-4 px1 px-sm-2 py2'>
										<div className='text--center'>
											<img src='/img/about/advisors/ruth-levine.jpg' className='col-sm-10 col-md-9'/>
										</div>
										<h4>
											<hr className='divider--sm' />
											<span className='title'>Ruth Levine</span>
										</h4>
										<p>William and Flora Hewlett Foundation</p>
									</div>

									<div className='col col-xs-6 col-sm-4 px1 px-sm-2 py2'>
										<div className='text--center'>
											<img src='/img/about/advisors/rachel-nugent.jpg' className='col-sm-10 col-md-9'/>
										</div>
										<h4>
											<hr className='divider--sm' />
											<span className='title'>Rachel Nugent</span>
										</h4>
										<p>Disease Control Priorities Network</p>
									</div>

									<div className='col col-xs-6 col-sm-4 px1 px-sm-2 py2'>
										<div className='text--center'>
											<img src='/img/about/advisors/richard-skolnik.jpg' className='col-sm-10 col-md-9'/>
										</div>
										<h4>
											<hr className='divider--sm' />
											<span className='title'>Richard Skolnik</span>
										</h4>
										<p>Yale University and Results for Development</p>
									</div>

									<div className='col col-xs-6 col-sm-4 px1 px-sm-2 py2'>
										<div className='text--center'>
											<img src='/img/about/advisors/damian-walker.jpg' className='col-sm-10 col-md-9'/>
										</div>
										<h4>
											<hr className='divider--sm' />
											<span className='title'>Damian Walker</span>
										</h4>
										<p>Bill and Melinda Gates Foundation</p>
									</div>

								</div>
							</div>

						</div>

					</div>
				</div>

			</div>
		);
	}
}

export default Team;
