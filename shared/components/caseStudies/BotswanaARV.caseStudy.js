
import React from 'react';
import ReactDOM from 'react-dom';
import Row from './utilities/Row';
import Col from './utilities/Column';
import Paragraph from './utilities/Paragraph';
import Break from './utilities/Break';
import Video from './utilities/Video';

import CaseStudyNavigation from './utilities/CaseStudyNavigation';
import MiniMap from './utilities/MiniMap';
import MiniVisualization from './utilities/MiniVisualization';

import CaseStudyCover from './utilities/CaseStudyCover';
import CaseStudySummary from './utilities/CaseStudySummary';

import BotswanaARVVisualization from './mainVisualizations/BotswanaARV.visualization';

import classNames from 'classnames';
import bootstrapSourcing from './utilities/sourcing';

//
// Botswana ARV Case Study
//
class Case extends React.Component {
	navigateToSection(section) {
		var targetSection = ReactDOM.findDOMNode(this.refs[section]);
		var offset = Math.abs(targetSection.offsetTop - window.pageYOffset);
		Velocity(targetSection, 'scroll', {duration: (1000/2600*offset), easing: 'swing', offset: -144});
	}

	componentDidMount() {
		document.title = this.props.caseStudy.title;
		
		if(!this.props.caseStudy.visualizationsLoaded) {
			this.props.loadMiniVisualizations(this.props.caseStudy);	
		}

		bootstrapSourcing(document.getElementsByTagName('sup'));
	}
	
	render() {

		var caseStudy = this.props.caseStudy;

		var background        = caseStudy.background,
				programRollout    = caseStudy.programRollout,
				impact            = caseStudy.impact,
				cost              = caseStudy.cost,
				reasonsForSuccess = caseStudy.reasonsForSuccess,
				implications      = caseStudy.implications;

		var sectionStyle = 'container--narrow px1 px-sm-2';
		var titleStyle   = 'title mt0';

		return (
			<div>

				<CaseStudyCover caseStudy={caseStudy} caseId={this.props.caseId} />

				<CaseStudySummary caseStudy={caseStudy} caseId={this.props.caseId} />

				<CaseStudyNavigation navigateToSection={this.navigateToSection.bind(this)}/>

				<div id='case-study-background'>
					<div className={classNames(sectionStyle, 'mt4')} ref='background' id='case-study-background'>
						<a name='background'></a>
						<Row>
							<Col>
								<h2 className={titleStyle}>Background</h2>
								<p className='lead'>{ caseStudy.lead }</p>
							</Col>
							<Col>
								<MiniMap region={ caseStudy.region } template={ caseStudy.template } />
							</Col>
						</Row>
					</div>

					<Video videoSource='https://www.youtube.com/embed/aulgLI1cHCE' videoTitle='HIV/AIDS in Botswana and the African Comprehensive HIV/AIDS Partnerships' videoType='youTube' videoCover='/img/case-studies/case-1/botswana-video-cover.jpg'/>

					<div className={sectionStyle}>
						<Row>
							<Col>
								<Paragraph markup={ background.paragraphOne } />
								<Paragraph markup={ background.paragraphTwo } />
							</Col>
							<Col>
								<MiniVisualization visualization={ background.visualizationOne } />
							</Col>
						</Row>
						<Row>
							<Col>
								<MiniVisualization visualization={ background.visualizationTwo } />
							</Col>
							<Col>
								<Paragraph markup={ background.paragraphThree } />
							</Col>
						</Row>
					</div>

					<Break break={caseStudy.breakOne} />
				</div>

				<div id='case-study-program-rollout'>
					<div className={sectionStyle} ref='programRollout'>
						<a name='program-rollout'></a>
						<Row>
							<Col>
								<h2 className={titleStyle}>Program Rollout</h2>
								<Paragraph markup={ programRollout.paragraphOne } />
								<Paragraph markup={ programRollout.paragraphTwo } />
							</Col>
							<Col>
								<MiniVisualization visualization={ programRollout.visualizationOne } />
							</Col>
						</Row>
						<Row>
							<Col>
								<MiniVisualization visualization={ programRollout.visualizationTwo } />
							</Col>
							<Col>
								<Paragraph markup={ programRollout.paragraphThree } />
								<Paragraph markup={ programRollout.paragraphFour } />
							</Col>
						</Row>
					</div>

					<div className='case-study-img-container clearfix'>
						<div className='case-study-img'>
							<div className='col col-md-12' style={{overflow:'hidden'}}>
								<img src='/img/case-studies/case-1/faust-botswana.jpg' />
							</div>
						</div>
						<div className='img-source' style={{top: 0, bottom: 'initial'}}>
							<a href='//aids.harvard.edu/faust-visits-bhp/president-faust-botswana/' target='_blank'>Photo credit: Justin Ide</a>
						</div>
						<div className={sectionStyle} style={{position:'relative'}}>
							<Row>
								<div className='col col-md-6 col-md-offset-6 px1 px-sm-2 bg--white case-study-img-text case-study-img-text--bottom'>
									<Paragraph markup={ programRollout.paragraphFive } />
								</div>
							</Row>
						</div>
					</div>

					<div className={sectionStyle}>
						<Row>
							<div className='col col-md-6 col-md-offset-6 px1 px-sm-2'>
								<Paragraph markup={ programRollout.paragraphSix } />
							</div>
						</Row>
					</div>

					<Break break={caseStudy.breakTwo} />
				</div>

				<div id='case-study-impact'>
					<div className={sectionStyle} ref='impact'>
						<a name='impact'></a>
						<Row>
							<Col>
								<h2 className={titleStyle}>Impact</h2>
								<Paragraph markup={ impact.paragraphOne } />
								<Paragraph markup={ impact.paragraphTwo } />
								<Paragraph markup={ impact.paragraphThree } />
							</Col>
							<Col>
								<MiniVisualization visualization={ impact.visualizationOne } />
							</Col>
						</Row>
						<Row>
							<Col>
								<MiniVisualization visualization={ impact.visualizationTwo } />
							</Col>
							<Col>
								<Paragraph markup={ impact.paragraphFour } />
							</Col>
						</Row>
					</div>

					<BotswanaARVVisualization source={caseStudy.mainVisualizationSource} />
				</div>

				<div id='case-study-cost'>
					<div className={sectionStyle} ref='cost'>
						<a name='cost'></a>
						<Row>
							<Col>
								<h2 className={titleStyle}>Cost</h2>
								<Paragraph markup={ cost.paragraphOne } />
							</Col>
							<Col>
								<MiniVisualization visualization={ cost.visualizationOne } isTogglable={true} />
							</Col>
						</Row>
						<Row>
							<Col>
								<MiniVisualization visualization={ cost.visualizationTwo } />
							</Col>
							<Col>
								<Paragraph markup={ cost.paragraphTwo } />
								<Paragraph markup={ cost.paragraphThree } />
								<Paragraph markup={ cost.paragraphFour } />
							</Col>
						</Row>
					</div>

					<Break break={caseStudy.breakThree} />
				</div>

				<div id='case-study-reasons-for-success'>
					<div className={sectionStyle} ref='reasonsForSuccess'>
						<a name='reasons-for-success'></a>
						<Row>
							<Col>
								<h2 className={titleStyle}>Reasons for Success</h2>
								<Paragraph markup={ reasonsForSuccess.paragraphOne } />
								<Paragraph markup={ reasonsForSuccess.paragraphTwo } />
							</Col>
							<Col>
								<MiniVisualization visualization={ reasonsForSuccess.visualizationOne } />
							</Col>
						</Row>
					</div>

					<div className='case-study-img-container mb3 clearfix'>
						<div className='case-study-img'>
							<div className='col col-md-12' style={{overflow:'hidden'}}>
								<img src='/img/case-studies/case-1/festus-mogae.jpg' />
							</div>
						</div>
						<div className='img-source'>
							<a href='//www.flickr.com/photos/worldbank/7638985624/in/photolist-cD2NhG-dYsqQT-6yU8Q2-dYsjv4-mDmmtD-mDo1AN-mDnZQ9-pyMMZ-4jAaqe-6v1Wkt-mDmmfn-mDmktx-mDnYQ3-9CcCk8-7J8ehH-qwML2u-eavv6m-7TdXe6-eapRtT-dsxwz3-7dDPgy-8K2qDr-5ahbxN-dYLjv7-eapRyk-e14yuR-6yU76T-dYLju9-cD2JvW-5rZHLA-DrLTe1-aEeXBX-5acVd8-fksjor-dYLjvG-8fT9ei-hETYaH-8fWk3L-8fWqe3-8K5sjy-fKLHZH-8K2ou2-CWWztD-8f9XUF-8fWoUw-5b7vg9-8McvkE-dshy96-8McF4u-6sXXdU' target='_blank'>Photo credit: World Bank</a>
						</div>
						<div className={sectionStyle} style={{position:'relative'}}>
							<Row>
								<div className='col col-md-6 col-md-offset-6 px1 px-sm-2 bg--white case-study-img-text'>
									<Paragraph markup={ reasonsForSuccess.paragraphThree } />
									<Paragraph markup={ reasonsForSuccess.paragraphFour } />
								</div>
							</Row>
						</div>
					</div>

					<div className={sectionStyle}>
						<Row>
							<div className='col col-md-6 col-md-offset-6 px1 px-sm-2'>
								<Paragraph markup={ reasonsForSuccess.paragraphFive } />
							</div>
						</Row>
					</div>

					<Break break={caseStudy.breakFour} />
				</div>

				<div id='case-study-implications'>
					<div className={sectionStyle} ref='implications'>
						<a name='implications'></a>
						<Row>
							<Col>
								<h2 className={titleStyle}>Implications</h2>
								<Paragraph markup={ implications.paragraphOne } />
								<Paragraph markup={ implications.paragraphTwo } />
							</Col>
							<Col>
								<MiniVisualization visualization={ implications.visualizationOne } />
							</Col>
						</Row>
						<Row>
							<Col>
								{ /* source: http://www.unicef.org/infobycountry/esaro.html */ }
								<MiniVisualization visualization={ implications.visualizationTwo } />
							</Col>
							<Col>
								<Paragraph markup={ implications.paragraphThree } />
								<Paragraph markup={ implications.paragraphFour } />
							</Col>
						</Row>
					</div>

					<div className='bg--teal'>
						<div className="conclusion-mini-map">
							<MiniMap region={caseStudy.region} template={caseStudy.template} conclusion={true} />
						</div>
					</div>

					<Break break={caseStudy.conclusion} conclusion={true} />
				</div>

			</div>
		);
	}
}

export default Case;
