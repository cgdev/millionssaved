
import React from 'react';
import ReactDOM from 'react-dom';
import Row from './utilities/Row';
import Col from './utilities/Column';
import Paragraph from './utilities/Paragraph';
import Break from './utilities/Break';
import Video from './utilities/Video';

import MiniVisualization from './utilities/MiniVisualization';
import MiniMap from './utilities/MiniMap';
import CaseStudyNavigation from './utilities/CaseStudyNavigation';

import CaseStudyCover from './utilities/CaseStudyCover';
import CaseStudySummary from './utilities/CaseStudySummary';

import KenyaOVCMainVisualization from './mainVisualizations/KenyaOVC.visualization';

import classNames from 'classnames';
import bootstrapSourcing from './utilities/sourcing';

//
// Kenya OVC Case Study
//
class Case extends React.Component {
	navigateToSection(section) {
		var targetSection = ReactDOM.findDOMNode(this.refs[section]);
		var offset = Math.abs(targetSection.offsetTop - window.pageYOffset);
		Velocity(targetSection, 'scroll', {duration: (1000/2600*offset), easing: 'swing', offset: -144});
	}

	componentDidMount() {
		document.title = this.props.caseStudy.title;
		
		if(!this.props.caseStudy.visualizationsLoaded) {
			this.props.loadMiniVisualizations(this.props.caseStudy);	
		}

		bootstrapSourcing(document.getElementsByTagName('sup'));
	}

	render() {

		var caseStudy = this.props.caseStudy;

		var background        = caseStudy.background,
				programRollout    = caseStudy.programRollout,
				impact            = caseStudy.impact,
				cost              = caseStudy.cost,
				reasonsForSuccess = caseStudy.reasonsForSuccess,
				implications      = caseStudy.implications;

		var sectionStyle = 'container--narrow px1 px-sm-2';
		var titleStyle   = 'title mt0';

		return (
			<div>

				<CaseStudyCover caseStudy={caseStudy} caseId={this.props.caseId} />

				<CaseStudySummary caseStudy={caseStudy} caseId={this.props.caseId} />

				<CaseStudyNavigation navigateToSection={this.navigateToSection.bind(this)}/>

				<div id='case-study-background'>
					<div className={classNames(sectionStyle, 'mt4')} ref='background' id='case-study-background'>
						<a name='background'></a>
						<Row>
							<Col>
								<h2 className={titleStyle}>Background</h2>
								<p className='lead'>{ this.props.caseStudy.lead }</p>
							</Col>
							<Col>
								<MiniMap region={this.props.caseStudy.region} template={this.props.caseStudy.template} />
							</Col>
						</Row>
					</div>

					<Video videoSource='https://www.youtube.com/embed/qPPdME1rtZo' videoTitle='Breaking the Cycle of Poverty in Kenya with Cash Transfers' videoType='youTube' videoCover='/img/case-studies/case-4/kenya-ovc-1.jpg'/>

					<div className={sectionStyle}>
						<Row>
							<Col>
								<MiniVisualization visualization={ this.props.caseStudy.background.visualizationOne } />
							</Col>
							<Col>
								<Paragraph markup={ this.props.caseStudy.background.paragraphOne } />
								<Paragraph markup={ this.props.caseStudy.background.paragraphTwo } />
							</Col>
						</Row>
					</div>

					<Break break={this.props.caseStudy.breakOne} />
				</div>

				<div id='case-study-program-rollout'>
					<div className={sectionStyle} ref='programRollout'>
						<a name='program-rollout'></a>
						<Row>
							<Col>
								<h2 className={titleStyle}>Program Rollout</h2>
								<Paragraph markup={ this.props.caseStudy.programRollout.paragraphOne } />
							</Col>
							<Col>
								<MiniVisualization visualization={ this.props.caseStudy.programRollout.visualizationOne } />
							</Col>
						</Row>
						<Row>
							<Col>
								<MiniVisualization visualization={ this.props.caseStudy.programRollout.visualizationTwo } />
							</Col>
							<Col>
								<Paragraph markup={ this.props.caseStudy.programRollout.paragraphTwo } />
								<Paragraph markup={ this.props.caseStudy.programRollout.paragraphThree } />
							</Col>
						</Row>
						<Row>
							<Col>
								<Paragraph markup={ this.props.caseStudy.programRollout.paragraphFour } />
								<Paragraph markup={ this.props.caseStudy.programRollout.paragraphFive } />
								<Paragraph markup={ this.props.caseStudy.programRollout.paragraphSix } />
							</Col>
							<Col>
								<MiniVisualization visualization={ this.props.caseStudy.programRollout.visualizationThree } />
							</Col>
						</Row>
						<Row>
							<Col>
								<MiniVisualization visualization={ this.props.caseStudy.programRollout.visualizationFour } />
							</Col>
							<Col>
								<Paragraph markup={ this.props.caseStudy.programRollout.paragraphSeven } />
								<Paragraph markup={ this.props.caseStudy.programRollout.paragraphEight } />
							</Col>
						</Row>
					</div>

					<Break break={this.props.caseStudy.breakTwo} />
				</div>

				<div id='case-study-impact'>
					<div className={sectionStyle} ref='impact'>
						<a name='impact'></a>
						<Row>
							<Col>
								<h2 className={titleStyle}>Impact</h2>
								<Paragraph markup={ this.props.caseStudy.impact.paragraphOne } />
								<Paragraph markup={ this.props.caseStudy.impact.paragraphTwo } />
							</Col>
						</Row>
					</div>

					<KenyaOVCMainVisualization source={caseStudy.mainVisualizationSource} />

					<div className={sectionStyle}>
						<Row>
							<div className='col col-md-6 col-md-offset-6 px1 px-sm-2'>
								<Paragraph markup={ this.props.caseStudy.impact.paragraphThree } />
								<Paragraph markup={ this.props.caseStudy.impact.paragraphFour } />
							</div>
						</Row>
					</div>

					<Break break={this.props.caseStudy.breakThree} />
				</div>

				<div id='case-study-cost'>
					<div className={sectionStyle} ref='cost'>
						<a name='cost'></a>
						<Row>
							<Col>
								<h2 className={titleStyle}>Cost</h2>
								<Paragraph markup={ this.props.caseStudy.cost.paragraphOne } />
								<Paragraph markup={ this.props.caseStudy.cost.paragraphTwo } />
							</Col>
							<Col>
								<MiniVisualization visualization={ this.props.caseStudy.cost.visualizationOne } />
							</Col>
						</Row>
						<Row>
							<Col>
								<MiniVisualization visualization={ this.props.caseStudy.cost.visualizationTwo } />
							</Col>
							<Col>
								<Paragraph markup={ this.props.caseStudy.cost.paragraphThree } />
								<Paragraph markup={ this.props.caseStudy.cost.paragraphFour } />
							</Col>
						</Row>
					</div>

					<Break break={this.props.caseStudy.breakFive} />
				</div>

				<div id='case-study-reasons-for-success'>
					<div className={sectionStyle} ref='reasonsForSuccess'>
						<a name='reasons-for-success'></a>
						<Row>
							<Col>
								<h2 className={titleStyle}>Reasons for Success</h2>
								<Paragraph markup={ this.props.caseStudy.reasonsForSuccess.paragraphOne } />
								<Paragraph markup={ this.props.caseStudy.reasonsForSuccess.paragraphTwo } />
							</Col>
							<Col>
								<MiniVisualization visualization={ this.props.caseStudy.reasonsForSuccess.visualizationOne } />
							</Col>
						</Row>
						<Row>
							<Col>
								<MiniVisualization visualization={ this.props.caseStudy.reasonsForSuccess.visualizationTwo } />
							</Col>
							<Col>
								<Paragraph markup={ this.props.caseStudy.reasonsForSuccess.paragraphThree } />
							</Col>
						</Row>
						<Row>
							<Col>
								<Paragraph markup={ this.props.caseStudy.reasonsForSuccess.paragraphFour } />
							</Col>
						</Row>
					</div>

					<Break break={this.props.caseStudy.breakFour} />
				</div>

				<div id='case-study-implications'>
					<div className={sectionStyle} ref='implications'>
						<a name='implications'></a>
						<Row>
							<Col>
								<h2 className={titleStyle}>Implications</h2>
								<Paragraph markup={ this.props.caseStudy.implications.paragraphOne } />
							</Col>
							<Col>
								<MiniVisualization visualization={ this.props.caseStudy.implications.visualizationOne } />
							</Col>
						</Row>

						<Row>
							<Col>
								<Paragraph markup={ this.props.caseStudy.implications.paragraphTwo } />
							</Col>
						</Row>
					</div>

					<div className='case-study-img-container mb3 clearfix'>
						<div className='case-study-img'>
							<div className='col col-md-12' style={{overflow:'hidden'}}>
								<img src='/img/case-studies/case-4/kenya-ovc-3.jpg' />
							</div>
						</div>
						<div className='img-source'>
							<a href='//www.flickr.com/photos/jeremywilburn/5233286537/in/photolist-8YrXLe-8YpQmp-8YEPeu-9RVQu-8Z45U6-8Z3hE8-8Ytory-8Yu1D9-8YrdTn-8Z68GS-8YGSds-8YAyok-djyro4-8YG1wN-8YHZmy-8YF5o7-8YEM3M-8YqVxD-8YqHYT-8YA9ip-djzsjy-bqrSSM-8YuVRY-8YuQuJ-8Z7dau-8YpCLi-8YpUVD-8YBqVa-8Jgquz-oFmUz5-8YDNiR-8YC2na-8YEakB-8YqM3F-8Z38Tn-8YtJP1-8YA3RL-4jvAos-8YrCFM-8YG857-djyMJx-8YuJoJ-djyoCr-8YpPaT-8YprH2-8YDFyw-8YGghQ-8YrSNP-8YDfaV-8YC3ye/' target='_blank'>Photo credit: Jeremy Wilburn</a>
						</div>
						<div className={sectionStyle} style={{position:'relative'}}>
							<Row>
								<div className='col col-md-6 px1 px-sm-2 bg--white case-study-img-text'>
									<Paragraph markup={ this.props.caseStudy.implications.paragraphThree } />
								</div>
							</Row>
						</div>
					</div>

					<div className={sectionStyle}>
						<Row>
							<div className='col col-md-6 col-md-offset-6 px1 px-sm-2'>
								<Paragraph markup={ this.props.caseStudy.implications.paragraphFour } />
							</div>
						</Row>
					</div>

					<div className='bg--teal'>
						<div className="conclusion-mini-map">
							<MiniMap region={caseStudy.region} template={caseStudy.template} conclusion={true} />
						</div>
					</div>

					<Break break={caseStudy.conclusion} conclusion={true} />
				</div>

			</div>
		);
	}
}

export default Case;
