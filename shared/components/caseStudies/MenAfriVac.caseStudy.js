
import React from 'react';
import ReactDOM from 'react-dom';
import Row from './utilities/Row';
import Col from './utilities/Column';
import Paragraph from './utilities/Paragraph';
import Break from './utilities/Break';
import Video from './utilities/Video';

import CaseStudyCover from './utilities/CaseStudyCover';
import CaseStudySummary from './utilities/CaseStudySummary';
import MiniVisualization from './utilities/MiniVisualization';
import MiniMap from './utilities/MiniMap';
import CaseStudyNavigation from './utilities/CaseStudyNavigation';

import MenAfriVacVisualization from './mainVisualizations/MenAfriVac.visualization';

import classNames from 'classnames';
import bootstrapSourcing from './utilities/sourcing';

//
// MenAfriVac Case Study
//
class Case extends React.Component {
	navigateToSection(section) {
		var targetSection = ReactDOM.findDOMNode(this.refs[section]);
		var offset = Math.abs(targetSection.offsetTop - window.pageYOffset);
		Velocity(targetSection, 'scroll', {duration: (1000/2600*offset), easing: 'swing', offset: -144});
	}

	componentDidMount() {
		document.title = this.props.caseStudy.title;
		
		if(!this.props.caseStudy.visualizationsLoaded) {
			this.props.loadMiniVisualizations(this.props.caseStudy);
		}

		bootstrapSourcing(document.getElementsByTagName('sup'));
	}

	render() {

		var caseStudy = this.props.caseStudy;

		var background        = caseStudy.background,
				programRollout    = caseStudy.programRollout,
				impact            = caseStudy.impact,
				cost              = caseStudy.cost,
				reasonsForSuccess = caseStudy.reasonsForSuccess,
				implications      = caseStudy.implications;

		var sectionStyle = 'container--narrow px1 px-sm-2';
		var titleStyle   = 'title mt0';

		return (
			<div>

				<CaseStudyCover caseStudy={caseStudy} caseId={this.props.caseId} />

				<CaseStudySummary caseStudy={caseStudy} caseId={this.props.caseId} />

				<CaseStudyNavigation navigateToSection={this.navigateToSection.bind(this)}/>

				<div id='case-study-background'>
					<div className={classNames(sectionStyle, 'mt4')} ref='background'>
						<a name='background'></a>
						<Row>
							<Col>
								<h2 className={titleStyle}>Background</h2>
								<p className='lead'>{ this.props.caseStudy.lead }</p>
							</Col>
							<Col>
								<MiniMap region={this.props.caseStudy.region} template={this.props.caseStudy.template} />
							</Col>
						</Row>
					</div>

					<Video videoSource='https://www.youtube.com/embed/QWoHqpw28C0' videoTitle='A New Vaccine for Africa' videoType='youTube' videoCover='/img/case-studies/case-0/menAfriVac-video-cover2.jpg' />

					<div className={sectionStyle}>
						<Row>
							<Col>
								<MiniVisualization visualization={ this.props.caseStudy.background.visualizationOne } />
							</Col>
							<Col>
								<Paragraph markup={ this.props.caseStudy.background.paragraphOne } />
								<Paragraph markup={ this.props.caseStudy.background.paragraphTwo } />
							</Col>
						</Row>
						<Row>
							<Col>
								<Paragraph markup={ this.props.caseStudy.background.paragraphThree } />
								<Paragraph markup={ this.props.caseStudy.background.paragraphFour } />
							</Col>
							<Col>
								<MiniVisualization visualization={ this.props.caseStudy.background.visualizationTwo } />
							</Col>
						</Row>
					</div>

					<Break break={this.props.caseStudy.breakOne} />
				</div>

				<div id='case-study-program-rollout'>
					<div className={sectionStyle} ref='programRollout'>
						<a name='program-rollout'></a>
						<Row>
							<Col>
								<MiniVisualization visualization={ this.props.caseStudy.programRollout.visualizationOne } />
							</Col>
							<Col>
								<h2 className={titleStyle}>Program Rollout</h2>
								<Paragraph markup={ this.props.caseStudy.programRollout.paragraphOne } />
								<Paragraph markup={ this.props.caseStudy.programRollout.paragraphTwo } />
							</Col>
						</Row>
						<Row>
							<Col>
								<Paragraph markup={ this.props.caseStudy.programRollout.paragraphThree } />
							</Col>
							<Col>
								<MiniVisualization visualization={ this.props.caseStudy.programRollout.visualizationTwo } />
							</Col>
						</Row>
						<Row>
							<Col>
								<Paragraph markup={ this.props.caseStudy.programRollout.paragraphFour } />
							</Col>
						</Row>
						<Row>
							<Col>
								<MiniVisualization visualization={ this.props.caseStudy.programRollout.visualizationThree } />
							</Col>
							<Col>
								<Paragraph markup={ this.props.caseStudy.programRollout.paragraphFive } />
								<Paragraph markup={ this.props.caseStudy.programRollout.paragraphSix } />
							</Col>
						</Row>
					</div>

					<Video videoSource='https://www.youtube.com/embed/xq7e5CBYAE4' videoTitle='Impact of MenAfriVac in Chad and Burkina Faso' videoType='youTube' videoCover='/img/case-studies/case-0/menAfriVac-video-cover1.jpg' />

					<div className={sectionStyle}>
						<Row>
							<Col>
								<Paragraph markup={ this.props.caseStudy.programRollout.paragraphSeven } />
							</Col>
							<Col>
								<MiniVisualization visualization={ this.props.caseStudy.programRollout.visualizationFour } />
							</Col>
						</Row>
						<Row>
							<Col>
								<MiniVisualization visualization={ this.props.caseStudy.programRollout.visualizationFive } />
							</Col>
							<Col>
								<Paragraph markup={ this.props.caseStudy.programRollout.paragraphEight } />
								<Paragraph markup={ this.props.caseStudy.programRollout.paragraphNine } />
							</Col>
						</Row>
					</div>

					<Break break={this.props.caseStudy.breakTwo} />
				</div>

				<div id='case-study-impact'>
					<div className={sectionStyle} ref='impact'>
						<a name='impact'></a>
						<Row>
							<Col>
								<h2 className={titleStyle}>Impact</h2>
								<Paragraph markup={ this.props.caseStudy.impact.paragraphOne } />
								<Paragraph markup={ this.props.caseStudy.impact.paragraphTwo } />
							</Col>
							<Col>
								<MiniVisualization visualization={ this.props.caseStudy.impact.visualizationOne } />
							</Col>
						</Row>
					</div>

					<MenAfriVacVisualization source={caseStudy.mainVisualizationSource} />

					<div className={sectionStyle}>
						<Row>
							<div className='col col-md-6 col-md-offset-6 px1 px-sm-2'>
								<Paragraph markup={ this.props.caseStudy.impact.paragraphThree } />
								<Paragraph markup={ this.props.caseStudy.impact.paragraphFour } />
							</div>
						</Row>
					</div>

					<Break break={this.props.caseStudy.breakThree} />
				</div>

				<div id='case-study-cost'>
					<div className={sectionStyle} ref='cost'>
						<a name='cost'></a>
						<Row>
							<Col>
								<h2 className={titleStyle}>Cost</h2>
								<Paragraph markup={ this.props.caseStudy.cost.paragraphOne } />
								<Paragraph markup={ this.props.caseStudy.cost.paragraphTwo } />
							</Col>
							<Col>
								<MiniVisualization visualization={ this.props.caseStudy.cost.visualizationOne } />
							</Col>
						</Row>
						<Row>
							<div className='col col-md-6 col-md-offset-6 px1 px-sm-2'>
								<Paragraph markup={ this.props.caseStudy.cost.paragraphThree } />
							</div>
						</Row>
					</div>

					<Break break={this.props.caseStudy.breakFour} />
				</div>

				<div id='case-study-reasons-for-success'>
					<div className={sectionStyle} ref='reasonsForSuccess'>
						<a name='reasons-for-success'></a>
						<Row>
							<Col>
								<h2 className={titleStyle}>Reasons for Success</h2>
								<Paragraph markup={ this.props.caseStudy.reasonsForSuccess.paragraphOne } />
								<Paragraph markup={ this.props.caseStudy.reasonsForSuccess.paragraphTwo } />
							</Col>
							<Col>
								<MiniVisualization visualization={ this.props.caseStudy.reasonsForSuccess.visualizationOne } />
							</Col>
						</Row>
						<Row>
							<Col>
								<MiniVisualization visualization={ this.props.caseStudy.reasonsForSuccess.visualizationTwo } />
							</Col>
							<Col>
								<Paragraph markup={ this.props.caseStudy.reasonsForSuccess.paragraphThree } />
							</Col>
						</Row>
					</div>

					<Break break={this.props.caseStudy.breakFive} />
				</div>

				<div id='case-study-implications'>
					<div className={sectionStyle} ref='implications'>
						<a name='implications'></a>
						<Row>
							<Col>
								<h2 className={titleStyle}>Implications</h2>
								<Paragraph markup={ this.props.caseStudy.implications.paragraphOne } />
							</Col>
							<Col>
								<MiniVisualization visualization={ this.props.caseStudy.implications.visualizationOne } />
							</Col>
						</Row>
						<Row>
							<Col>
								<MiniVisualization visualization={ this.props.caseStudy.implications.visualizationTwo } />
							</Col>
							<Col>
								<Paragraph markup={ this.props.caseStudy.implications.paragraphTwo } />
							</Col>
						</Row>
					</div>

					<div className='bg--teal'>
						<div className="conclusion-mini-map">
							<MiniMap region={caseStudy.region} template={caseStudy.template} conclusion={true} />
						</div>
					</div>

					<Break break={caseStudy.conclusion} conclusion={true} />
				</div>
			</div>
		);
	}
}

export default Case;

