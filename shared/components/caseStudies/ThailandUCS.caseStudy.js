
import React from 'react';
import ReactDOM from 'react-dom';
import Row from './utilities/Row';
import Col from './utilities/Column';
import Paragraph from './utilities/Paragraph';
import Break from './utilities/Break';
import Video from './utilities/Video';

import MiniMap from './utilities/MiniMap';
import MiniVisualization from './utilities/MiniVisualization';
import CaseStudyNavigation from './utilities/CaseStudyNavigation';

import CaseStudyCover from './utilities/CaseStudyCover';
import CaseStudySummary from './utilities/CaseStudySummary';

import ThailandUCSVisualization from './mainVisualizations/ThailandUCS.visualization';

import classNames from 'classnames';
import bootstrapSourcing from './utilities/sourcing';

//
// Thailand UCS Case Study
//
class Case extends React.Component {
	navigateToSection(section) {
		var targetSection = ReactDOM.findDOMNode(this.refs[section]);
		var offset = Math.abs(targetSection.offsetTop - window.pageYOffset);
		Velocity(targetSection, 'scroll', {duration: (1000/2600*offset), easing: 'swing', offset: -144});
	}

	componentDidMount() {
		document.title = this.props.caseStudy.title;
		
		if(!this.props.caseStudy.visualizationsLoaded) {
			this.props.loadMiniVisualizations(this.props.caseStudy);	
		}

		bootstrapSourcing(document.getElementsByTagName('sup'));
	}

	render() {

		var caseStudy = this.props.caseStudy;

		var background        = caseStudy.background,
				programRollout    = caseStudy.programRollout,
				impact            = caseStudy.impact,
				cost              = caseStudy.cost,
				reasonsForSuccess = caseStudy.reasonsForSuccess,
				implications      = caseStudy.implications;

		var sectionStyle = 'container--narrow px1 px-sm-2';
		var titleStyle   = 'title mt0';

		return (
			<div>

				<CaseStudyCover caseStudy={caseStudy} caseId={this.props.caseId} />

				<CaseStudySummary caseStudy={caseStudy} caseId={this.props.caseId} />

				<CaseStudyNavigation navigateToSection={this.navigateToSection.bind(this)}/>

				<div id='case-study-background'>
					<div className={classNames(sectionStyle, 'mt4')} ref='background'>
						<a name='background'></a>
						<Row>
							<Col>
								<h2 className={titleStyle}>Background</h2>
								<p className='lead'>{ this.props.caseStudy.lead }</p>
								<Paragraph markup={ this.props.caseStudy.background.paragraphOne } />
							</Col>
							<Col>
								<MiniMap region={this.props.caseStudy.region} template={this.props.caseStudy.template} />
							</Col>
						</Row>
					</div>

					<Video videoSource='https://www.youtube.com/embed/lNniPgEp2gY' videoTitle='Thailand&#39;s Move Towards Universal Health Coverage' videoType='youTube' videoCover='/img/case-studies/case-3/thailand-ucs-video-cover.jpg' />

					<div className={sectionStyle}>
						<Row>
							<Col>
								<MiniVisualization visualization={ this.props.caseStudy.background.visualizationOne } />
							</Col>
							<Col>
								<Paragraph markup={ this.props.caseStudy.background.paragraphTwo } />
								<Paragraph markup={ this.props.caseStudy.background.paragraphThree } />
							</Col>
						</Row>
					</div>

					<Break break={this.props.caseStudy.breakOne} />
				</div>

				<div id='case-study-program-rollout'>
					<div className={sectionStyle} ref='programRollout'>
						<a name='program-rollout'></a>
						<Row>
							<Col>
								<h2 className={titleStyle}>Program Rollout</h2>
								<Paragraph markup={ this.props.caseStudy.programRollout.paragraphOne } />
								<Paragraph markup={ this.props.caseStudy.programRollout.paragraphTwo } />
							</Col>
							<Col>
								<MiniVisualization visualization={ this.props.caseStudy.programRollout.visualizationOne } />
							</Col>
						</Row>
						<Row>
							<Col>
								<MiniVisualization visualization={ this.props.caseStudy.programRollout.visualizationTwo } />
							</Col>
							<Col>
								<Paragraph markup={ this.props.caseStudy.programRollout.paragraphThree } />
								<Paragraph markup={ this.props.caseStudy.programRollout.paragraphFour } />
							</Col>
						</Row>
						<Row>
							<Col>
								<Paragraph markup={ this.props.caseStudy.programRollout.paragraphFive } />
								<Paragraph markup={ this.props.caseStudy.programRollout.paragraphSix } />
							</Col>
							<Col>
								<MiniVisualization visualization={ this.props.caseStudy.programRollout.visualizationThree } />
							</Col>
						</Row>
						<Row>
							<Col>
								<MiniVisualization visualization={ this.props.caseStudy.programRollout.visualizationFour } />
							</Col>
							<Col>
								<Paragraph markup={ this.props.caseStudy.programRollout.paragraphSeven } />
								<Paragraph markup={ this.props.caseStudy.programRollout.paragraphEight } />
							</Col>
						</Row>
						<Row>
							<Col>
								<Paragraph markup={ this.props.caseStudy.programRollout.paragraphNine } />
							</Col>
							<Col>
								<MiniVisualization visualization={ this.props.caseStudy.programRollout.visualizationFive } />
							</Col>
						</Row>
					</div>

					<Break break={this.props.caseStudy.breakTwo} />
				</div>

				<div id='case-study-impact'>
					<div className={sectionStyle} ref='impact'>
						<a name='impact'></a>
						<Row>
							<Col>
								<h2 className={titleStyle}>Impact</h2>
								<Paragraph markup={ this.props.caseStudy.impact.paragraphOne } />
								<Paragraph markup={ this.props.caseStudy.impact.paragraphTwo } />
							</Col>
							<Col>
								<MiniVisualization visualization={ this.props.caseStudy.impact.visualizationOne } />
								<MiniVisualization visualization={ this.props.caseStudy.impact.visualizationTwo } />
							</Col>
						</Row>
					</div>
                    
					<div className={sectionStyle}>
						<Row>
							<div className='col col-md-6 col-md-offset-6 px1 px-sm-2'>
								<Paragraph markup={ this.props.caseStudy.impact.paragraphThree } />
							</div>
						</Row>
					</div>

					<Break break={this.props.caseStudy.breakThree} />
				</div>

				<div id='case-study-cost'>
					<div className={sectionStyle} ref='cost'>
						<a name='cost'></a>
						<Row>
							<Col>
								<h2 className={titleStyle}>Cost</h2>
								<Paragraph markup={ this.props.caseStudy.cost.paragraphOne } />
								<Paragraph markup={ this.props.caseStudy.cost.paragraphTwo } />
							</Col>
							<Col>
								<MiniVisualization visualization={ this.props.caseStudy.cost.visualizationOne } />
							</Col>
						</Row>
					</div>

					<Break break={this.props.caseStudy.breakFour} />
				</div>

				<div id='case-study-reasons-for-success'>
					<div className={sectionStyle} ref='reasonsForSuccess'>
						<a name='reasons-for-success'></a>
						<Row>
							<Col>
								<h2 className={titleStyle}>Reasons for Success</h2>
								<Paragraph markup={ this.props.caseStudy.reasonsForSuccess.paragraphOne } />
								<Paragraph markup={ this.props.caseStudy.reasonsForSuccess.paragraphTwo } />
							</Col>
						</Row>
					</div>

					<div className='case-study-img-container mb3 clearfix'>
						<div className='case-study-img'>
							<div className='col col-md-12' style={{overflow:'hidden'}}>
								<img src='/img/case-studies/case-3/thailand-meeting.jpg' />
							</div>
						</div>
						<div className='img-source'>
							<a href='//www.flickr.com/photos/usaidasia/6006465460/in/photolist-a9LGVj-a9LHXC-9NsCbe-Ff59n9-a9HWnp-a9LH4y-9NGokq-5TFUyC-dPw1zT-krBjEN-4AnooW-dec2LV-apVqLg-6vY2v-2ahiN-apVr7t-k35D5-e6JwKJ-4MUgix-5TomWJ-apY7Zd-oaBERH-7R4Vyf-oajfki-iaarnR-tpb4Q-9NDy6B-cWJFqj-9ZX5mx-kJame-7QaWQn-9NG7YG-7oX3CF-mgzbyo-mgzbT1-ee6UTS-9rFCGJ-c2TgZy-mgEi6H-apVqwD-aPhrjt-aPhpWF-apVKni-9eKQfH-czgH7h-aPhqzF-dPw1U6-apVrAT-7XPCT4-dWbQim' target='_blank'>Photo credit: USAID</a>
						</div>
						<div className={sectionStyle} style={{position:'relative'}}>
							<Row>
								<div className='col col-md-6 px1 px-sm-2 bg--white case-study-img-text'>
									<Paragraph markup={ this.props.caseStudy.reasonsForSuccess.paragraphThree } />
								</div>
							</Row>
						</div>
					</div>

					<div className={sectionStyle}>
						<Row>
							<Col>
								<MiniVisualization visualization={ this.props.caseStudy.reasonsForSuccess.visualizationOne } />
							</Col>
							<Col>
								<Paragraph markup={ this.props.caseStudy.reasonsForSuccess.paragraphFour } />
								<Paragraph markup={ this.props.caseStudy.reasonsForSuccess.paragraphFive } />
								<Paragraph markup={ this.props.caseStudy.reasonsForSuccess.paragraphSix } />
							</Col>
						</Row>
					</div>

					<Break break={this.props.caseStudy.breakFive} />
				</div>

				<div id='case-study-implications'>
					<div className={sectionStyle} ref='implications'>
						<a name='implications'></a>
						<Row>
							<Col>
								<h2 className={titleStyle}>Implications</h2>
								<Paragraph markup={ this.props.caseStudy.implications.paragraphOne } />
								<Paragraph markup={ this.props.caseStudy.implications.paragraphTwo } />
							</Col>
						</Row>
						<Row>
							<Col>
								<MiniVisualization visualization={ this.props.caseStudy.implications.visualizationOne } />
							</Col>
							<Col>
								<Paragraph markup={ this.props.caseStudy.implications.paragraphThree } />
								<Paragraph markup={ this.props.caseStudy.implications.paragraphFour } />
							</Col>
						</Row>
						<Row>
							<Col>
								<Paragraph markup={ this.props.caseStudy.implications.paragraphFive } />
								<Paragraph markup={ this.props.caseStudy.implications.paragraphSix } />
							</Col>
							<Col>
								<MiniVisualization visualization={ this.props.caseStudy.implications.visualizationTwo } />
							</Col>
						</Row>
					</div>

					<div className='bg--teal'>
						<div className="conclusion-mini-map">
							<MiniMap region={caseStudy.region} template={caseStudy.template} conclusion={true} />
						</div>
					</div>

					<Break break={caseStudy.conclusion} conclusion={true} />
				</div>

			</div>
		);
	}
}

export default Case;
