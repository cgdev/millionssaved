
import React from 'react';
import d3 from 'd3';

import colors from '../utilities/colors';

class Visualization extends React.Component {

	constructor(props) {
		super(props);

		this.showSource = this.showSource.bind(this);
		this.hideSource = this.hideSource.bind(this);
	}

	showSource() {
  	var source = this.refs.source;
  	source.style.display = 'block';
  	Velocity(source, {
  		opacity: [ 1, 0 ]
  	}, {
  		duration: 500
  	});
  }

  hideSource() {
  	var source = this.refs.source;
  	Velocity(source, {
  		opacity: [ 0, 1 ]
  	}, {
  		duration: 500,
  		complete: () => {
  			source.style.display = 'none';
  		}
  	});
  }

	render() {
		return (
			<div className='mb3 main-visualization-container bg--grey'>
				<div className='container--narrow px1 px-sm-2 pt2'>
					<div className='mx1 mx-sm-n2 clearfix'>
						<div className='col col-md-12 px1 px-sm-2'>
							<h2 className='headline mt0'>Plan Nacer's Effects on Neonatal Mortality and Low Birth Weight</h2>
						</div>
						<div className='col col-md-12 px1 px-sm-2'>
							<p>The overall effects of Plan Nacer on neonatal mortality are impressive. The impact of a clinic adopting the program on the neonatal mortality rate, regardless of individual beneficiary status is a 22% reduction in neonatal mortality. The impact on Plan Nacer beneficiaries is a 74% percent reduction.</p>
						</div>
					</div>
				</div>

				<div className='container--narrow px1 px-sm-2 pt2 pb4'>
					<div className='mx1 mx-sm-n2 clearfix'>
						<div className='col col-md-6 px1 px-sm-2 text--center'>
							<h3 className='title mt0'>Risk of Neonatal Death</h3>
							<div className='clearfix'>
								<div className='col col-md-6 px1 px-sm-2 text--center'>
									<p>Reduced by 22%</p>
									<PieChart percentage={0.22} radius={40} pieHighlightColor={colors.yellow500} pieBackgroundColor={colors.grey300} />
									<p><strong>non-beneficiaries</strong> at participating hospitals</p>
								</div>
								<div className='col col-md-6 px1 px-sm-2 text--center'>
									<p>Reduced by 74%</p>
									<PieChart percentage={0.74} radius={40} pieHighlightColor={colors.yellow500} pieBackgroundColor={colors.grey300} />
									<p><strong>beneficiaries</strong> at participating hospitals</p>
								</div>
							</div>
						</div>
						<div className='col col-md-6 px1 px-sm-2 text--center'>
							<h3 className='title mt0'>Risk of Low Birth Weight</h3>
							<div className='clearfix'>
								<div className='col col-md-6 px1 px-sm-2 text--center'>
									<p>Reduced by 9%</p>
									<PieChart percentage={0.09} radius={40} pieHighlightColor={colors.yellow500} pieBackgroundColor={colors.grey300} />
									<p><strong>non-beneficiaries</strong> at participating hospitals</p>
								</div>
								<div className='col col-md-6 px1 px-sm-2 text--center'>
									<p>Reduced by 19%</p>
									<PieChart percentage={0.19} radius={40} pieHighlightColor={colors.yellow500} pieBackgroundColor={colors.grey300} />
									<p><strong>beneficiaries</strong> at participating hospitals</p>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div className='main-visualization-source' ref='source'>
					<div className='source-content'>
						<div dangerouslySetInnerHTML={{ __html: this.props.source }}></div>
						<button className='btn bg--yellow px2 py1' onClick={this.hideSource}>Back to visualization</button>
					</div>
				</div>

				<div className='toolbar--bottom'>
					<div className='container--narrow px2 py1 clearfix'>
						<div className='col col-sm-4 col-sm-offset-8 text--right'>
							<button className='btn bg--yellow px1' onClick={this.showSource}>i</button>
						</div>
					</div>
				</div>

			</div>
		);
	}
}

class PieChart extends React.Component {
	constructor(props) {
		super(props);
	}
	dasharray(percentage) {
		var pieChartRadius = this.props.radius / 2;
		return `${percentage * 6.32 * pieChartRadius} ${6.32 * pieChartRadius}`;
	}
	render() {

		var svgStyles = {
			background: 'transparent',
			transform: 'rotate(-90deg)',
			maxWidth: ((this.props.radius + 20) * 4 + 'px')
		};

		var backgroundCircleStyles = {
			fill: this.props.pieBackgroundColor
		};

		var pieChartStyles = {
			fill: 'transparent',
			stroke: this.props.pieHighlightColor,
			strokeWidth: this.props.radius,
			strokeDasharray: this.dasharray(this.props.percentage)
		};

		return (
			<div ref='piechart'>
				<svg width='100%' viewBox={`0 0 ${(this.props.radius + 20) * 2} ${(this.props.radius + 20) * 2}`} style={svgStyles}>
					<circle cx={this.props.radius + 20} cy={this.props.radius + 20} r={this.props.radius - 10} style={backgroundCircleStyles}></circle>
					<circle cx={this.props.radius + 20} cy={this.props.radius + 20} r={this.props.radius / 2 } style={pieChartStyles}></circle>
				</svg>
			</div>
		);
	}
}

export default Visualization;
