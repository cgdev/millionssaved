
import React from 'react';
import d3 from 'd3';

import colors from '../utilities/colors';

import d3Tip from 'd3-tip';

class Visualization extends React.Component {

	constructor(props) {
    super(props);

    this.showSource = this.showSource.bind(this);
    this.hideSource = this.hideSource.bind(this);
  }

	componentDidMount() {

		var el = this.refs.visualization;
		var visualizationContainer = d3.select(el);

		var dataset = [
			{
				name: 'Proportion building toilet in past 2 years',
				treatment: 15.9,
				control: 13,

				treatmentAbs: 152,
				controlAbs: 124,

				treatmentMax: 956,
				controlMax: 952,
				text: 'reported building a toilet in the past 2 years',
				// change: 22.1,
				change: 23,
				changeText: 'more likely to build toilets.'

			},
			{
				name: 'Percent reporting open defecation',
				treatment: 48.8,
				control: 53.2,

				treatmentAbs: 610,
				controlAbs: 665,

				treatmentMax: 1250,
				controlMax: 1250,
				text: 'reported defecating in the open',
				// change: 8.3,
				change: 9,
				changeText: 'less likely to practice open defecation'

			},
			{
				name: 'Diarrhea 7-day prevalence',
				treatment: 2.4,
				control: 3.8,

				treatmentAbs: 23,
				controlAbs: 36,

				treatmentMax: 959,
				controlMax: 956,
				text: 'were likely to suffer from diarrhea',
				// change: 36.3,
				change: 30,
				changeText: 'lower prevalence of diarrhea'

			}
		];

		var heightScale = d3.scale.linear()
												.domain([0, 900])
												.range([0, 240]);

		var tip = d3Tip().attr('class', 'd3-tip').html((d, i) => {
			return `
				<div class='d3-tip-content text--center'>
					<p class='title m0'>${d.percent}%</p>
					<p class="m0 small">${d.text}</p>
					<p class="m0 small text--yellow">[${d.absolute} out of ${d.max}]</p>
				</div>
			`;
		});

		var groups = visualizationContainer.selectAll('.group__bars')
										.data(dataset)
										.each(function(d, i) {
											var _self = d3.select(this);

											var barDataset = [
												{
													percent: d.control,
													absolute: d.controlAbs,
													max: d.controlMax,
													text: d.text,
													change: d.change,
													changeText: d.changeText
												},
												{
													percent: d.treatment,
													absolute: d.treatmentAbs,
													max: d.treatmentMax,
													text: d.text,
													change: d.change,
													changeText: d.changeText
												}
											];

											var svg = _self.append('svg')
																		 .attr('height', 240)
																		 .attr('width', 200)
																		 .style('margin', '0 auto');

											svg.append('rect')
												 .attr('width', 40)
												 .attr('height', 240)
												 .attr('x', 50)
												 .attr('y', 0)
												 .attr('fill', colors.grey300)
												 .attr('stroke', colors.grey100)
												 .attr('stroke-width', 2);

											svg.append('rect')
												 .attr('width', 40)
												 .attr('height', 240)
												 .attr('x', 110)
												 .attr('y', 0)
												 .attr('fill', colors.grey300)
												 .attr('stroke', colors.grey100)
												 .attr('stroke-width', 2);

											svg.selectAll('.bar')
												 .data(barDataset)
												 .enter()
												 .append('rect')
												 	.attr('width', 40)
												 	.attr('height', (d) => heightScale(d.absolute))
												 	.attr('x', (d, i) => i > 0 ? 110 : 50)
												 	.attr('y', (d) => 240 - heightScale(d.absolute))
												 	// .attr('y', (d) => 0)
												 	.attr('fill', (d, i) => i > 0 ? colors.yellow500 : colors.teal500)
												 	.attr('stroke', colors.grey100)
												 	.attr('stroke-width', 2)
												 	.on('mouseover', tip.show)
													.on('mouseout', tip.hide);

											svg.call(tip);

											_self.append('div')
													 .style({
													 	position: 'absolute',
													 	bottom: `${heightScale(barDataset[1].absolute) + 16}px`,
													 	left: '50%',
													 	'padding-left': '60px',
													 	'line-height': '1.25'
													 })
													 .html((d) => `
													 		<strong class="headline">${d.change}%</strong>
													 		<p class="m0 small">${d.changeText}</p>
													 	`);

										});

	}

	showSource() {
    var source = this.refs.source;
    source.style.display = 'block';
    Velocity(source, {
      opacity: [ 1, 0 ]
    }, {
      duration: 500
    });
  }

  hideSource() {
    var source = this.refs.source;
    Velocity(source, {
      opacity: [ 0, 1 ]
    }, {
      duration: 500,
      complete: () => {
        source.style.display = 'none';
      }
    });
  }

	componentWillUnmount() {
		d3.select('.d3-tip').remove();
	}

	render() {
		return (
			<div className='main-visualization-container bg--grey mb3'>

				<div className='toolbar--top'>
					<div className='container--narrow px1 px-sm-2 pt2'>
						<div className='mx1 mx-sm-n2 clearfix'>
							<div className='col col-md-12 px1 px-sm-2'>
								<h2 className='headline m0'>Impact of the Total Sanitation and Sanitation Marketing Project<br /><span className='text--muted'><span className='text--yellow'>Triggered</span> and <span className='text--teal'>Non-triggered</span> Villages</span></h2>
							</div>
						</div>
					</div>
				</div>

				<div className='container--narrow px1 px-sm-2 pt2'>
					<div className='visualization--indonesia-sanitation clearfix' ref='visualization'>
						<div className='group'>
							<div className='group__bars'></div>
							<div className='bars-label pt1'>
								<p className='title mt0'>Building toilet<br /> in past 2 years</p>
							</div>
						</div>
						<div className='group'>
							<div className='group__bars'></div>
							<div className='bars-label pt1'>
								<p className='title mt0'>Reporting<br />open defecation</p>
								<p className='text--muted'>(Lower is better)</p>
							</div>
						</div>
						<div className='group'>
							<div className='group__bars'></div>
							<div className='bars-label pt1'>
								<p className='title mt0'>Diarrhea 7-day<br />prevalence</p>
							</div>
						</div>
					</div>
				</div>

				<div className='main-visualization-source' ref='source'>
          <div className='source-content'>
            <div dangerouslySetInnerHTML={{ __html: this.props.source }}></div>
            <button className='btn bg--yellow px2 py1' onClick={this.hideSource}>Back to visualization</button>
          </div>
        </div>

        <div className='toolbar--bottom'>
          <div className='container--narrow px2 py1 clearfix'>
            <div className='col col-sm-4 col-sm-offset-8 text--right'>
              <button className='btn bg--yellow px1' onClick={this.showSource}>i</button>
            </div>
          </div>
        </div>

			</div>
		);
	}
}

export default Visualization;
