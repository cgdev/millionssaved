
import React from 'react';
import d3 from 'd3';
import d3Tip from 'd3-tip';

import colors from '../utilities/colors';

class Visualization extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      activeEvent: undefined,
      eventsData: [
        { id: 0, active: false, vertical: -10, year: 1989, events: [ 'Ban on tobacco advertising' ]},
        { id: 1, active: false, vertical: -10, year: 1992, events: [ 'Ban on sale to children under 18, distribution of free samples, or sale via vending machines' ]},
        { id: 2, active: false, vertical: -15, year: 1992, events: [ 'Ban on advertisements for tobacco brands or contests/promotions linked to tobacco purchase' ]},
        { id: 3, active: false, vertical: -20, year: 1992, events: [ 'Ban on smoking on public transport and cineams; ban on smoking except for designated and/or private rooms in hospitals, schools, musuems, univiersities, libraries, government offices, banks, and more' ]},
        { id: 4, active: false, vertical: -10, year: 1993, events: [ 'Excise tax raised to 60%' ]},
        { id: 5, active: false, vertical: -10, year: 1998, events: [ 'New health warnings to cover one third of pack' ]},
        { id: 6, active: false, vertical: -10, year: 1999, events: [ 'Excise tax raised to 71.5%' ]},
        { id: 7, active: false, vertical: -10, year: 2001, events: [ 'Extra 2% \'health tax\' on cigarettes added to fund anti-tobacco health promotion' ]},
        { id: 8, active: false, vertical: -15, year: 2001, events: [ 'Ban of smoking portrayal on TV' ]},
        { id: 9, active: false, vertical: -10, year: 2003, events: [ 'Ban on smoking in air-conditioned restaurants' ]},
        { id: 10, active: false, vertical: -10, year: 2004, events: [ 'Thailand ratifies the FCTC' ]},
        { id: 11, active: false, vertical: -10, year: 2005, events: [ 'Ban on point of sale advertising', 'Graphic health warning to cover 50% of package' ]},
        { id: 12, active: false, vertical: -10, year: 2008, events: [ 'New earmarked 1.5% tax added to support Thai Public Broadcasting Service' ]},
        { id: 13, active: false, vertical: -10, year: 2009, events: [ 'Thailand becomes one of 16 LMICs to participate in the WHO\'s Global Adult Tobacco Survey (GATS)' ]},
        { id: 14, active: false, vertical: -15, year: 2009, events: [ 'Excise tax reaches 85% (after series of tax hikes)' ]},
        { id: 15, active: false, vertical: -10, year: 2010, events: [ 'Graphic health warning to cover 55% of package' ]},
        { id: 16, active: false, vertical: -15, year: 2010, events: [ 'Ban on smoking in all indoor public and work places, open-air public spaces, and open-air restaurants' ]},
        { id: 17, active: false, vertical: -10, year: 2014, events: [ 'Graphic health warning to cover 85% of the pack' ]},
      ]
    };

    this.showSource = this.showSource.bind(this);
    this.hideSource = this.hideSource.bind(this);
  }

	componentDidMount() {

		// var el = this.refs.visualization.getDOMNode();
		var el = this.refs.visualization;
		var visualizationContainer = d3.select(el);

		var width = el.clientWidth,
				height = 560,
				margins = { x: 50, y: 100 };

		var dataset = [
			{ year: 1991, smokersMale: 59.33, smokersFemale: 4.95 },
			{ year: 1996, smokersMale: 54.46, smokersFemale: 3.50 },
			{ year: 2001, smokersMale: 48.44, smokersFemale: 2.95 },
			{ year: 2004, smokersMale: 43.69, smokersFemale: 2.64 },
			{ year: 2006, smokersMale: 42.19, smokersFemale: 2.80 },
			{ year: 2007, smokersMale: 41.70, smokersFemale: 1.94 },
			{ year: 2009, smokersMale: 45.60, smokersFemale: 3.10 },
			{ year: 2011, smokersMale: 46.60, smokersFemale: 2.60 }
		];

		var dataset2 = [
			{ year: 1989, smokers: 25.9, min: 23.5, max: 28.4 },
			{ year: 1990, smokers: 25.8, min: 23.5, max: 28.3 },
			{ year: 1991, smokers: 25.8, min: 23.5, max: 28.2 },
			{ year: 1992, smokers: 25.8, min: 23.6, max: 28.1 },
			{ year: 1993, smokers: 25.7, min: 23.7, max: 27.7 },
			{ year: 1994, smokers: 25.6, min: 23.7, max: 27.4 },
			{ year: 1995, smokers: 25.5, min: 23.8, max: 27 },
			{ year: 1996, smokers: 25.2, min: 23.8, max: 26.6 },
			{ year: 1997, smokers: 24.4, min: 23.1, max: 25.7 },
			{ year: 1998, smokers: 23.6, min: 22.4, max: 24.7 },
			{ year: 1999, smokers: 22.9, min: 21.8, max: 24 },
			{ year: 2000, smokers: 22.1, min: 21.1, max: 23.2 },
			{ year: 2001, smokers: 21.5, min: 20.5, max: 22.5 },
			{ year: 2002, smokers: 20.7, min: 19.7, max: 21.7 },
			{ year: 2003, smokers: 20, min: 19.1, max: 21 },
			{ year: 2004, smokers: 19.6, min: 18.6, max: 20.6 },
			{ year: 2005, smokers: 19.3, min: 18.2, max: 20.3 },
			{ year: 2006, smokers: 19, min: 18, max: 20.1 },
			{ year: 2007, smokers: 18.9, min: 17.9, max: 20 },
			{ year: 2008, smokers: 18.9, min: 17.9, max: 20 },
			{ year: 2009, smokers: 18.9, min: 17.9, max: 19.9 },
			{ year: 2010, smokers: 19, min: 17.9, max: 20.1 },
			{ year: 2011, smokers: 19.1, min: 18, max: 20.3 },
			{ year: 2012, smokers: 19.2, min: 17.8, max: 20.6 }
		];

		var eventsData = this.state.eventsData;

		var xScale = d3.scale.linear()
								   .domain([1989, 2012])
								   .range([margins.x, width - margins.x]);

		var yScale = d3.scale.linear()
									 .domain([0, 80])
								   .range([height - margins.y, margins.y]);

		var svg = visualizationContainer.append('svg')
								.attr('width', width)
								.attr('height', height);

		var xAxis = d3.svg.axis()
							    .scale(xScale)
							    .tickFormat(d3.format('d'));
  
		var yAxis = d3.svg.axis()
    					    .scale(yScale)
    					    .orient('left')
                  .tickFormat(function(d) {
                    return d + '%';
                  });

    svg.append('g')
    	 .attr('class', 'axis x-axis')
    	 .attr('transform', 'translate(0, ' + (height - margins.y) + ')')
    	 .call(xAxis);

    svg.append('g')
    	 .attr('class', 'axis y-axis')
    	 .attr('transform', 'translate(' + margins.x + ', 0)')
    	 .call(yAxis);

    function createDeviationPath(dset) {
    	var path = `M${xScale(dset[0].year)},${yScale(dset[0].max)} `;

    	for (var i = 1 ; i <= dset.length - 1; i++) {
    		path += `L${xScale(dset[i].year)},${yScale(dset[i].max)} `;
    	}

    	for (var j = dset.length - 1; j >= 0; j--) {
    		path += `L${xScale(dset[j].year)},${yScale(dset[j].min)} `;
    	}

    	return path + 'Z';
    }

    var deviationArea = svg.append('path')
    								.attr('d', createDeviationPath(dataset2))
    								.attr('fill', colors.grey200);

    var combinedLine = d3.svg.line()
												 .x((d) => xScale(d.year))
												 .y((d) => yScale(d.smokers));

    var maleLine = d3.svg.line()
										 .x((d) => xScale(d.year))
										 .y((d) => yScale(d.smokersMale));

    var femaleLine = d3.svg.line()
											 .x((d) => xScale(d.year))
											 .y((d) => yScale(d.smokersFemale));

    svg.append('path')
    	 .attr('d', combinedLine(dataset2))
    	 .attr('stroke', colors.grey100)
    	 .attr('stroke-width', 6)
    	 .attr('fill', 'none');

    svg.append('path')
    	 .attr('d', combinedLine(dataset2))
    	 .attr('stroke', colors.yellow500)
    	 .attr('stroke-width', 2)
    	 .attr('fill', 'none');

    svg.append('path')
    	 .attr('d', maleLine(dataset))
    	 .attr('stroke', colors.teal500)
    	 .attr('stroke-width', 2)
    	 .attr('fill', 'none');

    svg.append('path')
    	 .attr('d', femaleLine(dataset))
    	 .attr('stroke', colors.teal500)
    	 .attr('stroke-width', 2)
    	 .attr('fill', 'none');

    var tip = d3Tip().attr('class', 'd3-tip').html((d) => {
			return `
				<div class='d3-tip-content text--center'>
					<p class='title m0'>${d.year}</p>
					<p class="m0">${d.smokers}%</p>
					<p class="m0"><small>[${d.min} - ${d.max}]</small></p>
				</div>
			`;
		});

		var eventsTip = d3Tip().attr('class', 'd3-tip').html((d) => {
			return `
				<div class='d3-tip-content text--center'>
					<p class='title m0'>${d.year}</p>
					<p class="m0">${d.events[0]}</p>
				</div>
			`;
		});

		svg.selectAll('.points-of-interest')
    	 .data(eventsData)
    	 .enter()
    	 .append('circle')
    	 	.attr('class', 'points-of-interest')
    	 	.attr('cx', (d) => xScale(d.year))
    	 	.attr('cy', (d) => yScale(d.vertical))
    	 	.attr('r', 6)
    	 	.attr('fill', colors.yellow500)
    	 	.attr('stroke', colors.grey100)
    	 	.attr('stroke-width', 2)
        .on('click', (d) => {
          this.setState({
            activeEvent: d.id
          });
        });

    svg.call(eventsTip);

    svg.append('text')
	  	.attr('x', xScale(dataset[0].year) - 20)
	  	.attr('y', yScale(dataset[0].smokersMale) - 10)
			.text('Male smokers');

		svg.append('text')
	  	.attr('x', xScale(dataset[0].year) - 20)
	  	.attr('y', yScale(dataset[0].smokersFemale) - 10)
			.text('Female smokers');

    svg.append('text')
      .attr('x', xScale(dataset[0].year) - 20)
      .attr('y', yScale(dataset2[0].smokers) - 10)
      .text('Average smokers (both genders)');

	}

  showSource() {
    var source = this.refs.source;
    source.style.display = 'block';
    Velocity(source, {
      opacity: [ 1, 0 ]
    }, {
      duration: 500
    });
  }

  hideSource() {
    var source = this.refs.source;
    Velocity(source, {
      opacity: [ 0, 1 ]
    }, {
      duration: 500,
      complete: () => {
        source.style.display = 'none';
      }
    });
  }

	componentWillUnmount() {
		d3.select('.d3-tip').remove();
	}

	render() {
		return (
			<div className='main-visualization-container bg--grey mb3'>
				<div className='toolbar--top'>
					<div className='container--narrow px1 px-sm-2 pt2'>
						<div className='mxn1 mx-sm-n2 clearfix'>
							<div className='col col-md-12 px1 px-sm-2'>
								<h2 className='headline m0'>Prevalence of Smoking<br /><span className='text--yellow'>Click on the milestones to learn more</span></h2>
							</div>
						</div>
					</div>
				</div>

				<div className='container--narrow px1 px-sm-2 pt4' style={{paddingBottom: this.state.activeEvent || this.state.activeEvent === 0 ? '1.6rem' : '6.4rem'}}>
					<div className='visualization--line-chart' ref='visualization'></div>
				</div>

        {this.state.eventsData.map((smokerEvent, i) => {
          return smokerEvent.id === this.state.activeEvent ? (
            <div key={i} style={{background:colors.grey200}}>
              <div className='container--narrow px1 px-sm-2 pt2 pb3 clearfix' style={{paddingLeft: '70px'}}>
                <div className='col col-sm-6'>  
                  <p className='title'><strong>{smokerEvent.year}</strong></p>
                  <p>{smokerEvent.events[0]}</p>
                </div>
              </div>
            </div>
          ) : '';
        })}

        <div className='main-visualization-source' ref='source'>
          <div className='source-content'>
            <div dangerouslySetInnerHTML={{ __html: this.props.source }}></div>
            <button className='btn bg--yellow px2 py1' onClick={this.hideSource}>Back to visualization</button>
          </div>
        </div>

        <div className='toolbar--bottom'>
          <div className='container--narrow px2 py1 clearfix'>
            <div className='col col-sm-4 col-sm-offset-8 text--right'>
              <button className='btn bg--yellow px1' onClick={this.showSource}>i</button>
            </div>
          </div>
        </div>
			</div>
		);
	}
}

export default Visualization;
