
import React from 'react';
import d3 from 'd3';

import colors from '../utilities/colors';

class Visualization extends React.Component {

	constructor(props) {
    super(props);

    this.state = {
    	dataset: [
				{ name: 'UNINS', description: 'Uninsured', pre: 5.85, post: 6.06, change: 0.0356, ppChange: 0.21 },
				{ name: 'MWS', description: 'Medical Welfare Scheme', pre: 7.12, post: 7.92, change: 0.114, ppChange: 0.8 },
				{ name: 'Control', description: 'Civil Servants Medical Benefits Scheme (CSMBS) and Social Security Scheme (SSS)', pre: 7.93, post: 7.89, change: -0.005, ppChange: -0.04 }
			]
    };

    this.showSource = this.showSource.bind(this);
    this.hideSource = this.hideSource.bind(this);
  }

	showSource() {
    var source = this.refs.source;
    source.style.display = 'block';
    Velocity(source, {
      opacity: [ 1, 0 ]
    }, {
      duration: 500
    });
  }

  hideSource() {
    var source = this.refs.source;
    Velocity(source, {
      opacity: [ 0, 1 ]
    }, {
      duration: 500,
      complete: () => {
        source.style.display = 'none';
      }
    });
  }

	render() {
		return (
			<div className='main-visualization-container bg--grey mb3'>
				<div className='container--narrow px1 px-sm-2 pt2'>
					<div className='mx1 mx-sm-n2 clearfix'>
						<div className='col col-md-12 px1 px-sm-2'>
							<h2 className='headline mt0'>The Universal Coverage Scheme Improves 12 Month Inpatient Utilization</h2>
						</div>
						<div className='col col-md-12 px1 px-sm-2'>
							<p>Consistent with the aims of the program of expanding healthcare access to the poor, the data shows that the increase in overall utilization is primarily driven by an increase in utilization by the Medical Welfare Scheme group.</p>
						</div>
					</div>
				</div>

				<div className='container--narrow px2 pt1 pb4 clearfix'>
					{this.state.dataset.map((bar, i) => {
						return (
							<div className='col col-sm-4' key={i}>
								<Bar name={bar.name} description={bar.description} percentage={bar.change} factor={4} />
							</div>
						);
					})}
				</div>

				<div className='main-visualization-source' ref='source'>
					<div className='source-content'>
						<div dangerouslySetInnerHTML={{ __html: this.props.source }}></div>
						<button className='btn bg--yellow px2 py1' onClick={this.hideSource}>Back to visualization</button>
					</div>
				</div>

				<div className='toolbar--bottom'>
					<div className='container--narrow px2 py1 clearfix'>
						<div className='col col-sm-4 col-sm-offset-8 text--right'>
							<button className='btn bg--yellow px1' onClick={this.showSource}>i</button>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

class Bar extends React.Component {
	constructor(props) {
		super(props);
	}
	render() {
		return (
			<div ref='barchart'>
				<svg width='100%' viewBox='0 0 300 320' style={{borderTop:'1px solid #e6e6e6',borderBottom:'1px solid #e6e6e6',maxWidth:'320px',margin:'0 auto'}}>
					<rect x='0' y='160' width='300' height='1' style={{fill:colors.teal500}}></rect>
					<g>
						<text x={150} y={160 - (this.props.factor * 160) * this.props.percentage - (this.props.percentage / Math.abs(this.props.percentage) * 20)} textAnchor='middle'>
							<tspan>{this.props.percentage * 100 + '%'}</tspan>
						</text>
					</g>				
					<rect x='130' y={this.props.percentage > 0 ? (160 - (this.props.factor * 160) * this.props.percentage) : 160} width='40' height={(this.props.factor * 160) * Math.abs(this.props.percentage)} style={{fill:this.props.percentage < 0 ? colors.yellow500 : colors.teal500}}></rect>
				</svg>
				<div className='text--center px1 py1'>
					<p className='title'><strong>{this.props.name}</strong></p>
					<p className='small'>{this.props.description}</p>
				</div>
			</div>
		);
	}
}

export default Visualization;
