
import React from 'react';

//
// Case Study Break
//
class Break extends React.Component {
	render() {
		var caseBreak = this.props.break || {};

		var isQuote = caseBreak.isQuote;
		var textLg = caseBreak.lg || undefined;
		var textMd = caseBreak.md || undefined;
		var textSm = caseBreak.sm || undefined;

		// var cleanedText = textLg ? textLg.replace(/<(?:.|\n)*?>/gm, '') : '';
		var shareUrl = 'http://millionssaved.cgdev.org';
		// var twitterUrl = 'http://twitter.com/intent/tweet?url=' + (shareUrl) + '&text=' + encodeURIComponent(cleanedText + ' @CGDev #millionssaved');

		var referer = `${shareUrl}${this.props.referer ? this.props.referer : ''}`;
		var cleanedText = textLg ? textLg.replace(/<(?:.|\n)*?>/gm, '') : '';
		var shareText = encodeURIComponent(`${cleanedText} @CGDev #millionssaved `);
		var twitterUrl = `http://twitter.com/intent/tweet?url=${referer}&text=${shareText}&original_referer=${referer}`;

		return (
			<div className={`break-container bg--teal ${this.props.conclusion ? 'conclusion' : ''}`}>
				
				<div className='container--narrow text--center px1 px-sm-2'>
					{(() => { if(textLg) return (<div className='break--lg display--2' dangerouslySetInnerHTML={{ __html: textLg }}></div>)}())}
					{(() => { if(textMd) return (<div className='break--md title' dangerouslySetInnerHTML={{ __html: textMd }}></div>)}())}
					{(() => { if(textSm) return (<div className='break--sm subhead' dangerouslySetInnerHTML={{ __html: textSm }}></div>)}())}
				</div>

				{(() => { if(caseBreak.tweetable) return (<div className='container--narrow text--center px1 px-sm-2'><a href={twitterUrl} target='_blank' className='btn btn--outline px1 py1'>Tweet</a></div>)}())}

			</div>
		);

	}
}

export default Break;
