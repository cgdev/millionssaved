
import React from 'react';

import ShareButton from './ShareButton';

class CaseStudyCover extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			coverImageHidden: false
		};
		this.imageLoaded = this.imageLoaded.bind(this);
	}
	componentDidMount() {
		var initializeScrollMagic = () => {
			var header = this.refs.csHeader;
			var csHeaderContent = this.refs.csHeaderContent;

			this.scene = new ScrollMagic.Scene({triggerElement: header, triggerHook: 'onLeave', duration: '100%', offset: 60})
					.addTo(window.scrollMagicMasterController)
					.on('progress', function(e) {
						window.requestAnimationFrame(() => {
							csHeaderContent.style.transform = `translateZ(0) translateY(${e.progress.toFixed(3) * 200}px)`;
							csHeaderContent.style.opacity = 1 - 1.5 * e.progress.toFixed(3);
						});
					});
		}

		initializeScrollMagic();
	}

	componentDidUpdate(prevProps) {
		var initializeScrollMagic = () => {
			var header = this.refs.csHeader;
			var csHeaderContent = this.refs.csHeaderContent;

			this.scene = new ScrollMagic.Scene({triggerElement: header, triggerHook: 'onLeave', duration: '100%', offset: 60})
					.addTo(window.scrollMagicMasterController)
					.on('progress', function(e) {
						window.requestAnimationFrame(() => {
							csHeaderContent.style.transform = `translateZ(0) translateY(${e.progress.toFixed(3) * 200}px)`;
							csHeaderContent.style.opacity = 1 - 1.5 * e.progress.toFixed(3);
						});
					});
		}

		if(prevProps.caseId !== this.props.caseId) {
			initializeScrollMagic();
		}
	}

	componentWillReceiveProps(nextProps) {
		if(nextProps.caseId !== this.props.caseId) {
			if(this.scene) {
				this.scene.destroy(true);
			}
			this.setState({coverImageHidden: true});
		}
	}

	shouldComponentUpdate(nextProps) {
		return nextProps.caseId !== this.props.caseId;
	}

	componentWillUnmount() {
		if(this.scene) {
			this.scene.destroy(true);
		}
	}

	imageLoaded() {
		this.setState({ coverImageHidden: false });
		this.forceUpdate();
	}

	render() {

		var caseStudy = this.props.caseStudy;

		var imageUrl;
		var imgNameArray;

		if(caseStudy.image && caseStudy.image.filename) {
			imgNameArray = caseStudy.image.filename.split('.jpg');
			imageUrl = `//millionssaved.s3-us-west-2.amazonaws.com/lg/${imgNameArray[0]}_lg.jpg`;
		}
		else if(caseStudy.template || caseStudy.template === 0) {
			imageUrl = `/img/case-studies/medium/case-study-${caseStudy.template}.jpg`
		}
		else {
			imageUrl = '';
		}

		var imageCredit = {
			name: caseStudy.imageCredit ? caseStudy.imageCredit.name : '',
			url:  caseStudy.imageCredit ? caseStudy.imageCredit.link : '',
		};

		return (
			<div className='banner banner--cs' ref='csHeader' key={this.props.caseId}>
				{(() => {
					return imageUrl ? (<img src={imageUrl} ref='headerImg' onLoad={this.imageLoaded}/>) : '';
				}())}
				<div className='img-source'>
					{(() => {
						return imageCredit.url ? <a href={imageCredit.url} target='_blank'>{imageCredit.name}</a> : <span>{imageCredit.name}</span>;
					}())}
				</div>
				<div className='banner-overlay' style={{background: this.state.coverImageHidden ? 'rgba(0,0,0,1)' : 'rgba(0,0,0,0.4)'}}>
					<div className='container--narrow y-center px1 px-sm-2'>
						<div className='mxn1 mx-sm-n2 clearfix' ref='csHeaderContent' id='csHeaderContent'>
							<header className='col col-md-12 px1 px-sm-2'>
								<p className='title element--0' ref='preTitle'>{ caseStudy.preTitle }</p>
								<h1 className='display--4 element--1' ref='title'>{ caseStudy.title }</h1>
							</header>
							<div className='col col-md-4 px1 px-sm-2 visible--xs' ref='social'>
								<div className='cs-social element--2'>
									<ShareButton shareButtonType='Twitter' shareText={`${caseStudy.preTitle}: ${caseStudy.title}`} referer={`/case-studies/${caseStudy.slug}`} />
									<ShareButton shareButtonType='Facebook' shareText={`${caseStudy.preTitle}: ${caseStudy.title}`} referer={`/case-studies/${caseStudy.slug}`} />
									<ShareButton shareButtonType='Mail' shareText={`${caseStudy.preTitle}: ${caseStudy.title}`} referer={`/case-studies/${caseStudy.slug}`} mailBody={caseStudy.intro} />
								</div>
							</div>
							<div className='col col-md-8 px1 px-sm-2 visible--xs' ref='intro'>
								<p className='lead element--2'>{ caseStudy.intro }</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export default CaseStudyCover;
