
import React from 'react';
import MiniVisualization from './MiniVisualization';

class CaseStudySummary extends React.Component {

	shouldComponentUpdate(nextProps) {
		return nextProps.caseId !== this.props.caseId;
	}

	render() {

		var caseStudy = this.props.caseStudy;

		return (
			<aside className='cs-banner case-study-overview bg--teal text--center'>
				<div className='container--narrow px1'>
					{(() => { if(caseStudy.caseType === 1) {
						return (
							<div className='mxn1 clearfix'>
								<div className='col col-sm-4 px1 pt2'>
									<h1 className='headline'>Approach</h1>
									<MiniVisualization visualization={caseStudy.summary ? caseStudy.summary.approach : ''}/>
									<p style={{fontSize: '1.4rem',marginBottom:'3.6rem'}}>{(() => { return caseStudy.summary ? caseStudy.summary.approachText : 'Approach text could go here, and be a couple of lines long. Something like this.' }())}</p>
								</div>
								<div className='col col-sm-4 px1 pt2' style={{background: 'rgba(0,0,0,0.2)'}}>
									<h1 className='headline'>Impact</h1>
									<MiniVisualization visualization={caseStudy.summary ? caseStudy.summary.impact : ''} />
									<p style={{fontSize: '1.4rem',marginBottom:'3.6rem'}}>{(() => { return caseStudy.summary ? caseStudy.summary.impactText : 'Impact text could go here, and be a couple of lines long. Something like this.' }())}</p>
								</div>
								<div className='col col-sm-4 px1 pt2'>
									<h1 className='headline'>Cost</h1>
									<MiniVisualization visualization={caseStudy.summary ? caseStudy.summary.cost : ''} caption={caseStudy.summary.costPopup} />
								</div>
							</div>
					)}}())}
					{(() => { if(caseStudy.summary && caseStudy.summary.text) {
						// return (
						// 	<div className='mxn1 clearfix'>
						// 		<div className='col col-md-12'>
						// 			<p className='lead mt2 mb3'>{caseStudy.summary.text}</p>
						// 		</div>
						// 	</div>
						// )
						return '';
					}}())}
				</div>
			</aside>
		);
	}
}

export default CaseStudySummary;
