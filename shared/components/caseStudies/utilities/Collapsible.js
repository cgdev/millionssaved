
import React from 'react';

var CircleElement = (props) => (<div className='circle-element y-center'>{props.sign}</div>);

class Collapsible extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      collapsed: false
    };

    this.toggleCollapse = this.toggleCollapse.bind(this);
  }
  toggleCollapse() {
    this.setState({
      collapsed: !this.state.collapsed
    });
  }
  componentDidMount() {
    this.setState({
      collapsed: true
    });
  }
  render() {

    var collapsibleStyle = {
      display: this.state.collapsed ? 'none' : 'block'
    };

    return (

      <div className='container--narrow px1 px-sm-2 py1 mb2 bg--white clearfix'>
        
        <div className='mxn1 mx-sm-n2 clearfix'>
          <div className='col col-md-12' onClick={this.toggleCollapse} style={{cursor:'pointer'}}>
            <div style={{position: 'absolute', top: 0, left: 0, height: '100%'}} className='col-sm-1 col-md-2 visible--sm'>
              <CircleElement sign={this.props.sign} />
            </div>
            <h2 className='title col-sm-9 col-sm-offset-1 col-md-offset-2 px1 px-md-0' style={{paddingRight:'48px'}}>{this.props.title}</h2>
            <div className='col-md-1 text--center' style={{position: 'absolute', top: 0, right: '10px', height: '100%'}}>
              <button className='btn bg--yellow y-center' style={{width: '3.2rem', borderRadius: '50%'}} onClick={this.toggleCollapse}><strong>{ this.state.collapsed ? '+' : '-' }</strong></button>
            </div>
          </div>
        </div>

        <div className='mxn1 mx-sm-n2 clearfix' style={collapsibleStyle}>
          <div className='col col-sm-10 col-md-8 col-sm-offset-1 col-md-offset-2 px1 px-md-0 pb2'>
            { this.props.children }
          </div>
        </div>

      </div>
    );
  }
}

export default Collapsible;
