
import React from 'react';

class GetBook extends React.Component {
  constructor() {
    super();
  }

  render() {
    return (
      <div>
        <div className='container px1 px-sm-2 get-the-book'>
          <div className='mxn1 mx-sm-n2 clearfix'>
            <div className='col col-md-6 col-md-offset-3 px1 px-sm-2 text--center'>
              <img src='/img/millions-saved-books.jpg' className='mt1' />
              {/* <h2 className='display--1'>Get the Book</h2> */}
              <h2>
                <span className='subhead text--yellow text--uppercase'>Millions Saved</span><br /><hr className='divider--sm' />
                <span className='display--2'>Get the Book</span>
              </h2>
              <p className='mb2'>Get your copy of Millions Saved! For further inquiries, <a href='mailto:publications@cgdev.org'>contact CGD</a></p>
              <a href='//www.amazon.com/Millions-Saved-Proven-Success-Global/dp/1933286881' target='_blank' className='btn bg--yellow px2 py1 mb4'>Get the Book</a>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default GetBook;
