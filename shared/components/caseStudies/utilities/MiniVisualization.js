
import React from 'react';
import isMobile from 'ismobilejs';

//
// Case Study Mini Visualization
//
class MiniVisualization extends React.Component {

	constructor() {
		super();

		this.state = {
			popupOpen: false
		};

		this.togglePopup = this.togglePopup.bind(this);
		this.openPopup = this.openPopup.bind(this);
		this.closePopup = this.closePopup.bind(this);
	}

	togglePopup() {
		var popupToggle = this.refs.popupToggle;
		var popup = this.refs.popup;
		var popupContent = this.refs.popupContent;

		if(this.state.popupOpen) {
			this.closePopup(popupToggle, popup, popupContent);
		}
		else {
			this.openPopup(popupToggle, popup, popupContent);
		}
	}

	openPopup(popupToggle, popup, popupContent) {
		popupToggle.innerText = 'x';

		Velocity(popupContent, { opacity: [ 1, 0 ] }, { duration: 300, easing: [0.215, 0.61, 0.355, 1] });

		Velocity(popup, {
			'border-radius': [ 0, '50%' ],
			scale: [ 1, 0.106 ],
			translateZ: 0,
			translateY: [ -40, 0 ]
		}, {
			duration: 300,
			easing: [0.215, 0.61, 0.355, 1],
			complete: () => {
				this.setState({popupOpen: true});
			}
		});
	}

	closePopup(popupToggle, popup, popupContent) {
		popupToggle.innerText = '?';

		Velocity(popupContent, { opacity: [ 0, 1 ] }, { duration: 300, easing: [0.215, 0.61, 0.355, 1] });

		Velocity(popup, {
			'border-radius': [ '50%', 0 ],
			scale: [ 0.106, 1 ],
			translateZ: 0,
			translateY: [ 0, -40 ]
		}, {
			duration: 300,
			easing: [0.215, 0.61, 0.355, 1],
			complete: () => {
				this.setState({popupOpen: false});
			}
		});
	}

	handleViewToggle() {

		var toggle = arguments[0];
		var viewID = toggle.attributes['data-view'].value;
		var visualization = arguments[1];

		var oldToggle = visualization.getElementsByClassName('toggle--active')[0];
		var oldView = visualization.getElementsByClassName('view--active')[0];
		var newView = visualization.getElementsByClassName('view--' + viewID)[0];

		oldToggle.classList.remove('toggle--active');
		toggle.classList.add('toggle--active');
		oldView.classList.remove('view--active');
		newView.classList.add('view--active');

	}

	componentDidMount() {
		var triggerElement = this.refs.visualization;
		if(isMobile.any) {
			triggerElement.className += 'mini-visualization--active';
		}
		else {
			this.scene = new ScrollMagic.Scene({triggerElement: triggerElement, triggerHook: 'onEnter', offset: 200})
					.addTo(window.scrollMagicMasterController)
					.setClassToggle(triggerElement, 'mini-visualization--active');
		}
	}

	componentDidUpdate() {
		if(!this.props.isTogglable) return;

		var triggerElement = this.refs.visualization;
		var toggles = triggerElement.getElementsByClassName('toggle');

		for (var i = 0; i < toggles.length; i++) {
			toggles[i].addEventListener('click', this.handleViewToggle.bind(this, toggles[i], triggerElement));
		}
	}

	componentWillUnmount() {
		if(this.scene) {
			this.scene.destroy(true);
		}
	}

	render() {
		var html = this.props.visualization ? this.props.visualization.html : '';
		return (
			<div className='mini-visualization' ref='visualization'>
				<div dangerouslySetInnerHTML={{ __html: html }}></div>
				{(() => { if(this.props.caption) { return (
					
					<button className='popup-toggle' onClick={this.togglePopup} ref='popupToggle'>?</button>
				
				)}}())}
				{(() => { if(this.props.caption) { return (

					<div className='popup' ref='popup'>
						<div className='popup__content px1 py2' ref='popupContent'>
							<div>{this.props.caption}</div>
						</div>
					</div>

				)}}())}
			</div>
		);
	}
}

export default MiniVisualization;
