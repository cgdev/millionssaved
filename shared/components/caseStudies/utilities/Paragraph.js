
import React from 'react';

//
// Case Study Paragraph
//
class Paragraph extends React.Component {
	render() {
		return (
			<div>
				<div dangerouslySetInnerHTML={{ __html: this.props.markup }} className={this.props.paragraphClass}></div>
			</div>
		);
	}
}

export default Paragraph;
