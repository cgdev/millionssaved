
import React from 'react';
import { Link } from 'react-router';

import findIndex from 'lodash/array/findIndex';

//
// Case Study Related Cases
//
class RelatedCases extends React.Component {

	componentDidMount() {
		var triggerElement = this.refs.relatedCases;

		var initializeScrollMagic = () => {
			this.scene = new ScrollMagic.Scene({triggerElement: triggerElement, triggerHook: 'onEnter', offset: 100})
					.addTo(window.scrollMagicMasterController)
					.setClassToggle(triggerElement, 'related-cases--active');
		}

		initializeScrollMagic();
	}

	componentWillUnmount() {
		if(this.scene) {
			this.scene.destroy(true);
		}
	}

	shouldComponentUpdate(nextProps) {
		return nextProps.caseId !== this.props.caseId;
	}

	componentWillReceiveProps(nextProps) {
		if(nextProps.caseId !== this.props.caseId) {
			if(this.scene) {
				this.scene.destroy(true);
			}
		}
	}

	componentDidUpdate(prevProps) {
		if(prevProps.caseId !== this.props.caseId) {
			var initializeScrollMagic = () => {
				var triggerElement = this.refs.relatedCases;
				this.scene = new ScrollMagic.Scene({triggerElement: triggerElement, triggerHook: 'onEnter', offset: 100})
						.addTo(window.scrollMagicMasterController)
						.setClassToggle(triggerElement, 'related-cases--active');
			}
			initializeScrollMagic();
		}
	}

	render() {

		var caseIndex = findIndex(this.props.caseStudiesListing, { slug: this.props.caseId });
		var nextCase = this.props.caseStudiesListing[caseIndex + 1] || this.props.caseStudiesListing[0];
		var prevCase = this.props.caseStudiesListing[caseIndex - 1] || this.props.caseStudiesListing[this.props.caseStudiesListing.length - 1];

		return (
			<aside className='related-cases' style={{overflow:'hidden'}} ref='relatedCases'>
				<div className='clearfix' style={{margin:'1px -1px'}}>
					<article className='col col-md-6 related-cases__prev' style={{padding:'1px'}}>
						<div className='bg--grey px1 px-sm-2 py2'>
							<p>{prevCase.preTitle}</p>
							<h1 className='headline'>
								<Link to={ `/case-studies/${prevCase.slug}`} >{prevCase.title}</Link>
							</h1>
						</div>
					</article>
					<article className='col col-md-6 related-cases__next' style={{padding:'1px'}}>
						<div className='bg--grey px1 px-sm-2 py2'>
							<p>{nextCase.preTitle}</p>
							<h1 className='headline'>
								<Link to={ `/case-studies/${nextCase.slug}`} >{nextCase.title}</Link>
							</h1>
						</div>
					</article>
				</div>
			</aside>
		);
	}
}

export default RelatedCases;
