
import React from 'react';
import Icon from '../../../lib/icons';

class ShareButton extends React.Component {
	constructor() {
		super();

		this.renderFacebook = this.renderFacebook.bind(this);
		this.renderTwitter = this.renderTwitter.bind(this);
		this.renderMail = this.renderMail.bind(this);
		this.rootUrl = this.rootUrl.bind(this);
	}

	rootUrl() {
		return 'http://millionssaved.cgdev.org';
	}

	renderTwitter() {

		// `http://twitter.com/intent/tweet?url=${referer}&text=${shareText}` ?

		var referer = `${this.rootUrl()}${this.props.referer}`;
		// var shareText = encodeURIComponent(`${this.props.shareText} - ${referer}`);
		// var url = `https://twitter.com/intent/tweet?text=${shareText}&original_referer=${referer}`;

		var cleanedText = this.props.shareText ? this.props.shareText.replace(/<(?:.|\n)*?>/gm, '') : '';
		var shareText = encodeURIComponent(`${cleanedText} @CGDev #millionssaved `);
		var url = `http://twitter.com/intent/tweet?url=${referer}&text=${shareText}&original_referer=${referer}`;

		return <a href={url} className='btn btn--outline px1' target='_blank'><Icon name='twitter' color='#ffffff'/>{ /* Tweet */ }</a>;
	}

	renderFacebook() {

		// `http://facebook.com/sharer/sharer.php?u=${referer}&title=${shareText}` ?

		var referer = `${this.rootUrl()}${this.props.referer}`;
		var url = `http://facebook.com/sharer/sharer.php?u=${referer}`;

		return <a href={url} className='btn btn--outline px1' target='_blank'><Icon name='facebook' color='#ffffff'/>{ /* Share */ }</a>;
	}

	renderMail() {

		// `mailto:?subject=' + encodeURIComponent('Flowminder: ' + scope.share.title) + '&body=' + encodeURIComponent('Flowminder: ' + scope.share.title) + ' — ' + (shareUrl + scope.share.slug)` ?
		
		var subject = encodeURIComponent('Center for Global Development: Millions Saved');
		var referer = `${this.rootUrl()}${this.props.referer}`;
		var body = encodeURIComponent(`${this.props.shareText}\n\n${this.props.mailBody}\n\n${referer}`);
		var url = `mailto:?subject=${subject}&body=${body}`;

		return <a href={url} className='btn btn--outline px1'><Icon name='mail' color='#ffffff'/> { /* Mail */ }</a>;
	}

	render() {

		var shareBtn = this[`render${this.props.shareButtonType}`];

		return shareBtn();
	}
}

export default ShareButton;
