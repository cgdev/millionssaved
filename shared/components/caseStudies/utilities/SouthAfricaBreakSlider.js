
import React from 'react';

class BreakSlider extends React.Component {
	constructor() {
		super();

		this.state = {
			next: false,
			prev: false,
			currentSlide: 0,
			lastSlide: 0
		};

		this.nextSlide = this.nextSlide.bind(this);
		this.prevSlide = this.prevSlide.bind(this);
	}
	nextSlide() {
		this.setState({
			next: true,
			prev: false,
			currentSlide: this.state.currentSlide < this.props.slides.length - 1 ? this.state.currentSlide + 1 : 0,
			lastSlide: this.state.currentSlide
		});
	}
	prevSlide() {
		this.setState({
			next: false,
			prev: true,
			currentSlide: this.state.currentSlide > 0 ? this.state.currentSlide - 1 : this.props.slides.length - 1,
			lastSlide: this.state.currentSlide
		});
	}
	componentDidUpdate() {

		if(this.state.lastSlide == this.state.currentSlide) {
			return;
		}

		var currentSlide = document.getElementById(`break-slide-${this.state.currentSlide}`);
		var lastSlide = document.getElementById(`break-slide-${this.state.lastSlide}`);

		currentSlide.style['z-index'] = 2;
		lastSlide.style['z-index'] = 1;

		if(this.state.next) {
			Velocity(currentSlide, {
				translateZ: 0,
				translateX: [ 0, '100%' ],
				opacity: [ 1, 0 ]
			}, {
				duration: 500
			});
			Velocity(lastSlide, {
				translateZ: 0,
				translateX: [ '-100%', 0 ],
				opacity: [ 0, 1 ]
			}, {
				duration: 500
			});
		}
		else {
			Velocity(currentSlide, {
				translateZ: 0,
				translateX: [ 0, '-100%' ],
				opacity: [ 1, 0 ]
			}, {
				duration: 500
			});
			Velocity(lastSlide, {
				translateZ: 0,
				translateX: [ '100%', 0 ],
				opacity: [ 0, 1 ]
			}, {
				duration: 500
			});
		}
	}
	render() {

		return (
			<div className='break-container bg--teal'>
				<div className='container--narrow text--center px1 px-sm-2'>
					<div className='break--lg display--2'><p>Racial disparity in South Africa post-apartheid</p></div>
					<div className='break-slides-container'>
						{this.props.slides.map((item, i) => {
							return (
								<div className={this.state.currentSlide == i ? 'break-slide active' : 'break-slide'} key={i} id={`break-slide-${i}`}>
									<div className='break--md title'>
										<p>{item.verbose}</p>
									</div>
									{(() => {if(item.note) return (<div className='break--sm subhead' dangerouslySetInnerHTML={{ __html: item.note }}></div>)}())}
								</div>
							);
						})}
					</div>
					<button className='btn' onClick={this.prevSlide}>
						<svg width='36px' height='20px' viewBox='0 0 36 20'>
							<g transform="translate(0, 3)" fill='#ffffff'>
								<path d="M0.568,8.927 C0.568,8.506 0.752,8.143 1.040,7.869 L6.823,0.754 C7.331,0.130 8.260,0.029 8.896,0.537 C9.534,1.032 9.635,1.945 9.126,2.569 L5.143,7.475 L34.502,7.475 C35.316,7.475 35.977,8.129 35.977,8.927 C35.977,9.725 35.316,10.379 34.502,10.379 L5.143,10.379 L9.126,15.270 C9.635,15.911 9.534,16.811 8.896,17.317 C8.625,17.536 8.299,17.637 7.976,17.637 C7.543,17.637 7.114,17.448 6.823,17.085 L1.040,9.974 C0.752,9.712 0.568,9.349 0.568,8.927"></path>
							</g>
						</svg>
					</button>
					<button className='btn' onClick={this.nextSlide}>
						<svg width='36px' height='20px' viewBox='0 0 36 20'>
							<g transform="translate(0, 3)" fill='#ffffff'>
								<path d="M35.431,8.927 C35.431,8.506 35.247,8.143 34.959,7.869 L29.176,0.754 C28.668,0.130 27.739,0.029 27.103,0.537 C26.465,1.032 26.364,1.945 26.873,2.569 L30.856,7.475 L1.497,7.475 C0.683,7.475 0.022,8.129 0.022,8.927 C0.022,9.725 0.683,10.379 1.497,10.379 L30.856,10.379 L26.873,15.270 C26.364,15.911 26.465,16.811 27.103,17.317 C27.374,17.536 27.700,17.637 28.023,17.637 C28.456,17.637 28.885,17.448 29.176,17.085 L34.959,9.974 C35.247,9.712 35.431,9.349 35.431,8.927"></path>
							</g>
						</svg>
					</button>
				</div>
			</div>
		);
	}
}

export default BreakSlider;
