
export default function(csHeaderContent, callback, fixed) {
	if(!csHeaderContent) return;
	window.requestAnimationFrame(() => {
		if(!csHeaderContent[0]) return;
		Velocity(csHeaderContent[0], {
			translateZ: 0,
			translateY: [ 0, fixed ? 100 : '100%' ],
			opacity: [ 1, 0 ]
		}, {
			delay: 0,
			duration: 1200,
			easing: [ 0.77, 0, 0.175, 1 ]
		});
		if(!csHeaderContent[1]) return;
		Velocity(csHeaderContent[1], {
			translateZ: 0,
			translateY: [ 0, fixed ? 100 : '100%' ],
			opacity: [ 1, 0 ]
		}, {
			delay: 100,
			duration: 1200,
			easing: [ 0.77, 0, 0.175, 1 ]
		});
		if(!csHeaderContent[2]) return;
		Velocity(csHeaderContent[2], {
			translateZ: 0,
			translateY: [ 0, fixed ? 100 : '100%' ],
			opacity: [ 1, 0 ]
		}, {
			delay: 200,
			duration: 1200,
			easing: [ 0.77, 0, 0.175, 1 ],
			complete: () => {
				callback();
			}
		});
		if(!csHeaderContent[3]) return;
		Velocity(csHeaderContent[3], {
			translateZ: 0,
			translateY: [ 0, fixed ? 100 : '100%' ],
			opacity: [ 1, 0 ]
		}, {
			delay: 200,
			duration: 1200,
			easing: [ 0.77, 0, 0.175, 1 ]
		});
	});
}
