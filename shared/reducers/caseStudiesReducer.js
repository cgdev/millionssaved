
import objectAssign from 'object-assign';
import findWhere from 'lodash/collection/findWhere';

import {
  REQUEST_CASE_STUDIES, RECEIVE_CASE_STUDIES,
  REQUEST_CASE_STUDY, RECEIVE_CASE_STUDY,
  CHANGE_LIST_STYLE, CHANGE_REGION_FILTER,
  CHANGE_APPROACH_FILTER
} from '../actions/caseStudyActions';

import {
	REQUEST_MINI_VISUALIZATIONS,
	RECEIVE_MINI_VISUALIZATIONS
} from '../actions/miniVisualizationActions';

export default function caseStudiesReducer(state = {
	isFetching: false,
	isFetchingSingle: false,
	activeIndex: 0,
	caseStudies: [],
	fullCaseStudies: [],
	listDisplayStyle: 'grid',
	approachFilter: 'all',
	regionFilter: 'all'
}, action) {
	switch(action.type) {
		case REQUEST_CASE_STUDIES:
			return objectAssign({}, state, {
				isFetching: true
			});
		case RECEIVE_CASE_STUDIES:
			return objectAssign({}, state, {
				isFetching: false,
				caseStudies: action.caseStudies,
				lastUpdated: action.receivedAt
			});
		case REQUEST_CASE_STUDY:
			return objectAssign({}, state, {
				isFetchingSingle: true
			});
		case RECEIVE_CASE_STUDY:

			var newCaseStudiesArray = state.fullCaseStudies.concat([action.caseStudy]);
			var activeIndex = newCaseStudiesArray.indexOf(action.caseStudy);

			return objectAssign({}, state, {
				isFetchingSingle: false,
				activeIndex: activeIndex,
				fullCaseStudies: newCaseStudiesArray,
				lastUpdated: action.receivedAt
			});
		case CHANGE_LIST_STYLE:
			return objectAssign({}, state, {
				listDisplayStyle: action.listStyle
			});
		case CHANGE_REGION_FILTER:
			return objectAssign({}, state, {
				regionFilter: action.regionFilter
			});
		case CHANGE_APPROACH_FILTER:
			return objectAssign({}, state, {
				approachFilter: action.approachFilter
			});
		// case REQUEST_MINI_VISUALIZATIONS:
		// 	return state;
		case RECEIVE_MINI_VISUALIZATIONS:
			var targetCase = findWhere(state.fullCaseStudies, { _id: action.visualizations._id});

			var combiner = {
				background: objectAssign({}, targetCase.background, action.visualizations.background),
				programRollout: objectAssign({}, targetCase.programRollout, action.visualizations.programRollout),
				impact: objectAssign({}, targetCase.impact, action.visualizations.impact),
				cost: objectAssign({}, targetCase.cost, action.visualizations.cost),
				reasonsForSuccess: objectAssign({}, targetCase.reasonsForSuccess, action.visualizations.reasonsForSuccess),
				implications: objectAssign({}, targetCase.implications, action.visualizations.implications),
				visualizationsLoaded: true
			};

			var newCase = objectAssign({}, targetCase, combiner);
			var newCases = state.fullCaseStudies;
			newCases[state.fullCaseStudies.indexOf(targetCase)] = newCase;

			return objectAssign({}, state, {
				fullCaseStudies: newCases,
				lastUpdated: action.receivedAt
			});
		default:
			return state;
	}
}
