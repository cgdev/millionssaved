
import objectAssign from 'object-assign';

import {
  KENYA_CHANGE_YEAR,
  KENYA_CHANGE_DATASET
} from '../actions/kenyaVisualizationActions';

export default function kenyaVisualizationReducer(state = {
	currentYear: 0,
	currentDataset: 0,
	datasets: [
		{
			name: 'Households consuming meat in the past 7 days',
			treatment: [ 35, 71 ],
			control: [ 44, 62 ],
			pro: 'Consumed meat',
			con: 'Didn\'t consume meat'
		},
		{
			name: 'Households consuming milk in last 7 days',
			treatment: [ 44, 59 ],
			control: [ 53, 52 ],
			pro: 'Consumed milk',
			con: 'Didn\'t consume milk'
		},
		{
			name: 'Households consuming fruit in last 7 days',
			treatment: [ 38, 55 ],
			control: [ 47, 57 ],
			pro: 'Consumed fruit',
			con: 'Didn\'t consume fruit'
		},
		{
			name: 'Households living on less than $1/day',
			treatment: [ 37, 20 ],
			control: [ 33, 30 ],
			pro: 'Less than $1/day',
			con: 'More than $1/day'
		},
		{
			name: 'Households where kids aged 6-17 ever attended school',
			treatment: [ 91, 93 ],
			control: [ 92, 91 ],
			pro: 'Attended school',
			con: 'Didn\'t attend school'
		},
		{
			name: 'Households where teens aged 14-17 years enrolled in secondary school',
			treatment: [ 14, 20 ],
			control: [ 16, 15 ],
			pro: 'Enrolled',
			con: 'Didn\'t enroll'
		}
	]
}, action) {
	switch(action.type) {
		case KENYA_CHANGE_YEAR:
			return objectAssign({}, state, {
				currentYear: action.newYear
			});
		case KENYA_CHANGE_DATASET:
			return objectAssign({}, state, {
				currentDataset: action.newDataset
			});
		default:
			return state;
	}
}
