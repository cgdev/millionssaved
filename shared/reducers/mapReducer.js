
import objectAssign from 'object-assign';

import {
  REQUEST_MAP,
  RECEIVE_MAP
} from '../actions/mapActions';

export default function caseStudiesReducer(state = {
	isFetching: false,
	worldMap: {}
}, action) {
  switch(action.type) {
    case REQUEST_MAP:
			return objectAssign({}, state, {
        isFetching: true
			});
		case RECEIVE_MAP:
			return objectAssign({}, state, {
        isFetching: false,
				worldMap: action.worldMap
			});
    default:
      return state;
  }
}
