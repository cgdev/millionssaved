
import objectAssign from 'object-assign';
import findWhere from 'lodash/collection/findWhere';

import {
  REQUEST_MEDIA_ITEMS, RECEIVE_MEDIA_ITEMS
} from '../actions/mediaItemActions';

export default function mediaItemsReducer(state = {
  isFetching: false,
  mediaItems: []
}, action) {
  switch(action.type) {
    case REQUEST_MEDIA_ITEMS:
      return objectAssign({}, state, {
        isFetching: true
      });
    case RECEIVE_MEDIA_ITEMS:
      return objectAssign({}, state, {
        isFetching: false,
        mediaItems: action.mediaItems,
        lastUpdated: action.receivedAt
      });
    default:
      return state;
  }
}
