
import objectAssign from 'object-assign';

import {
  MENAFRIVAC_CHANGE_YEAR,
  MENAFRIVAC_CHANGE_DATASET
} from '../actions/menAfriVacVisualizationActions';

export default function kenyaVisualizationReducer(state = {
	currentYear: 0,
	currentDataset: 'cases',
	datasets: [
		{
			id: 'BEN',
			name: 'Benin',
			location: [2.29, 10],
			// 2009 - 2014
			cases:  [ 416, 323, 269, 1165, 833, 711 ],
			deaths: [ 64, 54, 50, 112, 82, 88 ],
			hyperendemic: false
		},
		{
			id: 'BFA',
			name: 'Burkina Faso',
			location: [-1.857214, 12.598451],
			// 2009 - 2014
			cases:  [ 4723, 6732, 3875, 6957, 2917, 3476 ],
			deaths: [ 629, 949, 588, 709, 339, 353 ],
			hyperendemic: true
		},
		{
			id: 'CIV',
			name: 'Cote d\'Ivoire',
			location: [-6.46, 6.07],
			// 2009 - 2014
			cases:  [ 284, 146, 141, 500, 255, 196 ],
			deaths: [ 46, 28, 25, 59, 34, 25 ],
			hyperendemic: false
		},
		{
			id: 'CMR',
			name: 'Cameroon',
			location: [12.58, 3.37],
			// 2009 - 2014
			cases:  [ 1001, 835, 2548, 542, 1010, 1156 ],
			deaths: [ 122, 71, 165, 64, 68, 60 ],
			hyperendemic: false
		},
		{
			id: 'CAF',
			name: 'Central African Republic',
			location: [21.92, 6.04],
			// 2009 - 2014
			cases:  [ 289, 477, 361, 266, 210, 169 ],
			deaths: [ 48, 106, 46, 31, 31, 41 ],
			hyperendemic: false
		},
		{
			id: 'TCD',
			name: 'Chad',
			location: [20.25, 16.09],
			// 2009 - 2014
			cases:  [ 1460, 3228, 5960, 3874, 371, 235 ],
			deaths: [ 152, 248, 270, 163, 40, 22 ],
			hyperendemic: true
		},
		{
			id: 'GHA',
			name: 'Ghana',
			location: [-1.37, 5.59],
			// 2009 - 2014
			cases:  [ 302, 1164, 773, 739, 454, 448 ],
			deaths: [ 63, 128, 57, 75, 41, 17 ],
			hyperendemic: false
		},
		{
			id: 'MLI',
			name: 'Mali',
			location: [-3.95, 19.68],
			// 2009 - 2014
			cases:  [ 335, 482, 430, 688, 358, 327 ],
			deaths: [ 28, 34, 17, 12, 7, 4 ],
			hyperendemic: true
		},
		{
			id: 'NGR',
			name: 'Niger',
			location: [8.58, 18.05],
			// 2009 - 2014
			cases:  [ 13449, 2908, 1214, 314, 311, 327 ],
			deaths: [ 558, 251, 145, 56, 44, 40 ],
			hyperendemic: true
		},
		{
			id: 'NGA',
			name: 'Nigeria',
			location: [8, 9.15],
			// 2009 - 2014
			cases:  [ 56128, 4983, 1165, 1206, 871, 1175 ],
			deaths: [ 2488, 337, 62, 74, 47, 81 ],
			hyperendemic: true
		},
		{
			id: 'TGO',
			name: 'Togo',
			location: [1.05, 7.43],
			// 2009 - 2014
			cases:  [ 350, 460, 440, 408, 266, 351 ],
			deaths: [ 47, 94, 43, 35, 22, 14 ],
			hyperendemic: false
		}
	]
}, action) {
	switch(action.type) {
		case MENAFRIVAC_CHANGE_YEAR:
			return objectAssign({}, state, {
				currentYear: action.newYear
			});
		case MENAFRIVAC_CHANGE_DATASET:
			return objectAssign({}, state, {
				currentDataset: action.newDataset
			});
		default:
			return state;
	}
}
