
import objectAssign from 'object-assign';

import {
  REQUEST_POSTS, RECEIVE_POSTS
} from '../actions/postActions';

export default function postReducer(state = {
	isFetching: false,
	posts: []
}, action) {
	switch(action.type) {
		case REQUEST_POSTS:
			return objectAssign({}, state, {
				isFetching: true
			});
		case RECEIVE_POSTS:
			return objectAssign({}, state, {
				isFetching: false,
				posts: action.posts,
				lastUpdated: action.receivedAt
			});
		default:
			return state;
	}
}
