
import React  from 'react';
import { Route, IndexRoute } from 'react-router';

import App       from './components/App';
import Home      from './components/Home';
import About     from './components/About';
import Media     from './components/Media';
import Findings  from './components/Findings';
import FAQ       from './components/FAQ';
import PostList  from './components/PostList';
import CaseStudiesList  from './components/CaseStudiesList';
import CaseStudy from './components/CaseStudy';

import Background from './components/about/Background';
import Cases from './components/about/Cases';
import Team from './components/about/Team';
import EmbeddableMap from './components/EmbeddableMap';

// const routes = (
// 	<Route path="/" component={App}>
// 		<IndexRoute component={Home}></IndexRoute>
// 		<Route path="about" component={About} />
// 		<Route path="findings" component={Findings} />
// 		<Route path="frequently-asked-questions" component={FAQ} />
// 		<Route path="case-studies" component={CaseStudiesList} />
// 		<Route path="case-studies/:caseId" component={CaseStudy} />
// 	</Route>
// );

const routes = (
	<Route path='/' component={App}>
		<IndexRoute component={Home} socialMediaTitle='Millions Saved'></IndexRoute>
		<Route path='about' component={About} socialMediaTitle='About Millions Saved'>
			<IndexRoute component={Background} socialMediaTitle='About Millions Saved — Background'/>
			<Route path='cases' component={Cases} socialMediaTitle='About Millions Saved — The Cases'/>
			<Route path='team' component={Team} socialMediaTitle='About Millions Saved — The Team'/>
		</Route>
		<Route path='findings' component={Findings} socialMediaTitle='Millions Saved — Key Findings'/>
		<Route path='frequently-asked-questions' component={FAQ} socialMediaTitle='Millions Saved — FAQ'/>
		<Route path='media' component={Media} socialMediaTitle='Millions Saved — Media'/>
		<Route path='embeddable-map' component={EmbeddableMap} socialMediaTitle='Embeddable Map'/>
		<Route path='case-studies/:caseId' component={CaseStudy} socialMediaTitle='Case Study'/>
	</Route>
);

export default routes;
