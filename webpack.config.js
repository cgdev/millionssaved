
require('dotenv').load();
var webpack = require('webpack');

module.exports = {
	entry: './client',
	output: {
		path: __dirname + '/public/js',
		filename: 'client.js'
	},
	module: {
		loaders: [
			{
				test: /\.jsx?$/,
				exclude: /(node_modules|bower_components)/,
				loader: 'babel'
				// query: {
				// 	optional: ['optimisation.react.constantElements', 'optimisation.react.inlineElements']
				// }
			}
		]
	},
	plugins: [
		// new webpack.optimize.DedupePlugin()
		new webpack.DefinePlugin({GA_TRACKING_CODE: process.env.NODE_ENV === 'production' ? JSON.stringify('UA-1044429-7') : JSON.stringify('UA-1044429-7')})
	]
};
